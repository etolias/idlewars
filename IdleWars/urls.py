from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url

from frontend.views import *

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    
    # ADMIN PAGE
    url(r'^admin/', include(admin.site.urls)),
    
    # API
    (r'^api/', include('API.urls')),
   
    # EVENTS
    #(r'^bust/(?P<bust_hash>[-\w]+)$',bust_computer),
    (r'^bust/(?P<bust_hash>[-\w]+)$',bust_computer2),
    (r'^bustbycode/$', bust_by_code),
    #(r'^rawinput/(?P<com_name>[-\w]+)/events', create_event),
    #(r'^rawinput/(?P<com_name>[-\w]+)/events', create_event2),
    (r'^rawinput/(?P<com_name>[-\w\ ]+)/events', create_event2),


    #\w|
    # FRONTEND
    url(r'^$', home_view, name='home'),
    (r'^leaderboards_screen_original/',leaderboards_screen_view),
    (r'^leaderboards_screen_pi/',leaderboards_screen_view2),

    (r'^leaderboards_screen/',leaderbards_screen_view_iframe),

    
    # REGISTRATION
    url(r'^accounts/register/$', UserProfileRegistrationClass.as_view(),{'backend': 'registration.backends.default.DefaultBackend'}, name='registration_register'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    (r'^verifyuser/$',verify_user),

    # DEBUG
    (r'^debug/all_bars/?$', all_timelines_view),

    (r'^debug/ganttchart/$', gantt_chart_view),
    (r'^debug/ganttchart2/$', gantt_chart_view2),

    (r'^debug/graph/$', graph_view),

    (r'^installationinstructions?$', show_installation_instructions),

    (r'^playinstructions?$', show_play_instructions),
    
    (r'^debug/?$', timeline_view),

)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# static files (images, css, javascript, etc.)
#urlpatterns += patterns('',
#        (r'^uploads/(?P<path>.*)$', 'django.views.static.serve', {
#        'document_root': settings.MEDIA_ROOT}))