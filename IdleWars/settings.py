#encoding:UTF-8
# Django settings for IdleWars project.
# modified by Enrico Costanza for custom server setup
# see http://hci.ecs.soton.ac.uk/wiki/ServerBasics

import os, socket
from fabric.api import run, env, prefix

ROOT_PATH = os.path.dirname(__file__)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Evangelos TOLIAS', 'et2e10@ecs.soton.ac.uk'),
    ('Enrico COSTANZA', 'ec@ecs.soton.ac.uk'),
    #('Another Person', 'another.person@something.com'),
)

MANAGERS = ADMINS

deploymentHosts = ['aicvm-et2e10','game']
localDeploymentHosts = ['hostname as returned by socket.gethostname()']
hostname = socket.gethostname()

if hostname in deploymentHosts:
    hosting = 'deployment'
elif hostname in localDeploymentHosts:
    hosting = 'localdeployment'
else:
    hosting = 'development'

DEPLOYMENT_NAME = "IdleWars"
#ßDEPLOYMENT_NAME = "IdleWars"
DEPLOYMENT_SUBNET = 'ecs.soton.ac.uk'

PROTOCOL = 'https'

if hosting == 'deployment':
    #ROOT_URL_PREFIX = '%s://%s.%s/%s/' % (PROTOCOL, hostname, DEPLOYMENT_SUBNET, DEPLOYMENT_NAME)
    ROOT_URL_PREFIX = '%s://%s.%s/%s/' % (PROTOCOL, 'game', DEPLOYMENT_SUBNET, DEPLOYMENT_NAME)
elif hosting == 'localdeployment':
    ROOT_URL_PREFIX = 'http://127.0.0.1:3380/' + DEPLOYMENT_NAME + '/'
else:
    ROOT_URL_PREFIX = 'http://127.0.0.1:8000/'
ROOT_URL = ROOT_URL_PREFIX

if hosting in ('deployment','localdeployment'):
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'IdleWars',
            #'NAME': 'IdleWars',
            'USER': 'postgres',
            'PASSWORD': 'arnaki',
            'HOST': 'localhost', # Set to empty string for localhost.
            'PORT': '', # Set to empty string for default.
        }
    }
    print "deployment"
#BASE_URL = DEPLOYMENT_NAME
#print "Using postgres"
else:
    # DATABASES = {
    #     'default': {
    #         'ENGINE': 'django.db.backends.sqlite3',
    #         'NAME': os.path.join(ROOT_PATH, '../../sqlite.db'), # Or path to database file if using sqlite3.
    #         'USER': '',     # Not used with sqlite3.
    #         'PASSWORD': '', # Not used with sqlite3.
    #         'HOST': '',     # Set to empty string for localhost. Not used with sqlite3.
    #         'PORT': '',     # Set to empty string for default. Not used with sqlite3.
    #     }
    # }
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'IdleWars',
            #'NAME': 'IdleWars',
            'USER': 'postgres',
            'PASSWORD': 'arnaki',
            'HOST': 'localhost', # Set to empty string for localhost.
            'PORT': '', # Set to empty string for default.
        }
    }
    BASE_URL = ''
    print "Non deployment"


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/London'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-uk'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(ROOT_PATH, 'uploads/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ROOT_URL_PREFIX + 'uploads/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(ROOT_PATH, 'media')

# for local host THIS NEEDS TO BE RELATIVE!!!
# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
if hosting == 'development':
    STATIC_URL = '/media/'
else:
    STATIC_URL = ROOT_URL_PREFIX + 'media/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    #os.path.join(ROOT_PATH, '..', 'static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = ')aq_gg#p(s58*le9%(rs9!s%bp6xv^ok!6=!rk+&amp;ayzt5d@t7_'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

if hosting in ('deployment', 'localdeployment'):
    ROOT_URLCONF = 'IdleWars' + '.urls'
else:
    ROOT_URLCONF = 'IdleWars' + '.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = DEPLOYMENT_NAME + '.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(ROOT_PATH, 'templates')
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.static',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'basicutils.djutils.populate_context',
    'frontend.context_processors.default_context'
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'frontend',
    'API',
    'registration',
    'django_nose',
    #'IdleWars',
)

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

ACCOUNT_ACTIVATION_DAYS = 7 # One-week activation window; you may, of course, use a different value.
REGISTRATION_OPEN =True
# for send_mail
if hosting == 'development':
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEFAULT_FROM_EMAIL = "webmaster@%s.%s" % (hostname, DEPLOYMENT_SUBNET)
SERVER_EMAIL = "webmaster@%s.%s" % (hostname, DEPLOYMENT_SUBNET)

# for registration module
ACCOUNT_ACTIVATION_DAYS = 3
DEFAULT_FROM_EMAIL = "webmaster@%s.%s" % (hostname, DEPLOYMENT_SUBNET)

# for forum
#FORUM_BASE = '/forum'

AUTH_PROFILE_MODULE = "custom_registration.RegistrationData"

# URL that handles the LOGIN path when running in local machine
LOGIN_URL = ROOT_URL_PREFIX + 'accounts/login'
    
# URL that handles the LOGOUT path when running in local machine
LOGOUT_URL = ROOT_URL_PREFIX + ''
    
# URL that handles the LOGIN path when running in local machine
LOGIN_REDIRECT_URL = ROOT_URL_PREFIX


if hosting == 'development':
    logFilename = 'usage.log'
else:
    logFilename = '/srv/log/' + DEPLOYMENT_NAME + '/usage.log'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'json': {
            'format': '{"level": "%(levelname)s", "timestamp": "%(asctime)s", %(message)s}'
        },
        'simple': {
            'format': '%(levelname)s %(asctime)s %(message)s'
        },
        'detailed': {
            'format': "%(levelname)s %(asctime)s \n%(pathname)s %(filename)s \n%(funcName)s \n%(message)s"
        },
                   
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file':{
            'level':'DEBUG',
            'class':'logging.FileHandler',
            'formatter': 'simple',
            'filename': logFilename
        },
        'file_json':{
            'level':'DEBUG',
            'class':'logging.FileHandler',
            'formatter': 'json',
            'filename': logFilename
        },
        'mail_admins': {
            'level': 'ERROR',
            'formatter': 'detailed',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'root': {
        'handlers': ['console', 'file', 'mail_admins'],
        'level': 'DEBUG'
    },
    'loggers': {
        'custom': {
            'handlers': ['console', 'file'],
            'level': 'DEBUG',
            'propagate': True,
        },

        'django': {
            'handlers':['console'],
            'level':'INFO',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['null'],
            'level': 'INFO',
            'propagate': True,
        },

        'custom': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
        'errors': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        }
    }
}
try:
    from django.db import connection
    if "django_site" in connection.introspection.table_names():
        from django.contrib.sites.models import Site
        mysite = Site.objects.get_current()
        print "mysite=", mysite
        mysite.domain = ROOT_URL_PREFIX
        print "mysite.domain", mysite.domain
        mysite.name = DEPLOYMENT_NAME
        print "mysite.name", mysite.name
        mysite.save()



except Exception as e:
    print "(warning) mysite variable has not been initialized (Note: if you are setting up the web site for the first time this is normal)"


print "os.getpid() =", os.getpid()
print "ROOT_PATH" , ROOT_PATH
print "hostname", hostname
print "STATIC_URL", STATIC_URL
#print "BASE_URL", BASE_URL
print "DEPLOYMENT_NAME", DEPLOYMENT_NAME
print "ROOT_URLCONF", ROOT_URLCONF
print "os.path.dirname(__file__)", os.path.dirname(__file__)
