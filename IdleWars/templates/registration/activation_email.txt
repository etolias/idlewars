Hi,
Thanks for registering to the IdleWars game. Please activate your account by clicking or copy and pasting the following link to your browser.
{{site}}accounts/activate/{{ activation_key }}/

Best regards,
IdleWars team
