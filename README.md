(Please note) Facebook authentication does not work because facebook updated the OAuth token policy see: http://stackoverflow.com/questions/16224141/can-not-authorise-user-in-facebook-js-sdk-this-authorization-code-has-been-us

# Server installation instruction (for Ubuntu) #
## Tools for building code ##
*Get information on the newest versions of packages and their dependencies.*


```
#!bash
sudo apt-get update

```


*Install build essential software*


```
#!bash

sudo apt-get install build-essential
sudo apt-get install python-dev python-imaging libncurses5-dev libffi-dev libssl-dev
```
*Install the database*

```
#!bash

sudo apt-get install postgresql
sudo apt-get install postgresql-contrib
sudo apt-get install python-psycopg2 libpq-dev (required for installing psycopg2 from the requirements)
```
*(this step can be done automaticaly fromt he fab script)The installation procedure created a user account called postgres that is associated with the default Postgres role. In order to use Postgres, we'll need to log into that account. You can do that by typing:*
```
#!bash
sudo -i -u postgres
createdb EcoScreenCatcher
psql
\password postgres
#set password for the postgres role
\q
```

*Install the web servers*

```
#!bash

sudo apt-get install nginx
sudo apt-get install apache2
sudo apt-get install libapache2-mod-wsgi
sudo a2enmod wsgi
```
*Edit /etc/apache2/ports.conf to make it listen to 8080*
```
#!dash

nano /etc/apache2/ports.conf
```

*Create /etc/nginx/proxy.conf with the following content:*



```
#!configuration

proxy_redirect off;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Protocol $scheme;
client_max_body_size 10m;
client_body_buffer_size 128k;
proxy_connect_timeout 90;
proxy_send_timeout 90;
proxy_read_timeout 90;
proxy_buffer_size 4k;
proxy_buffers 4 32k;
proxy_busy_buffers_size 64k;
proxy_temp_file_write_size 64k;
```

*Edit /etc/apache2/sites-available/000-default.conf to EXACTLY look like the following (Remove all other entries but the following):*

```
#!configuration

    # The ServerName directive sets the request scheme, hostname and port that
	# the server uses to identify itself. This is used when creating
	# redirection URLs. In the context of virtual hosts, the ServerName
	# specifies what hostname must appear in the request's Host: header to
	# match this virtual host. For the default virtual host (this file) this
	# value is not decisive as it is used as a last resort host regardless.
	# However, you must set it for any further virtual host explicitly.
	#ServerName www.example.com
ServerName 8.8.8.8

<VirtualHost *:8080>
  ServerAdmin <your email here>

  LogLevel warn

  ###### Django projects

  Include /etc/apache2/django-projects/*

</VirtualHost>
```


*Edit /etc/nginx/sites-available/default to look like the following (if https is not required):*

```
#!configuration

server {
  listen *:80;
  server_name www.SERVERNAME.com SERVERNAME.com;

  location / {
    root /var/www/nginx-default;
    index index.html index.htm;
  }

  location /server-status/ {
    proxy_pass http://localhost:8080;
    include /etc/nginx/proxy.conf;
  }

  # django projects
  include django-projects/*.http;

}

```
*All the server related material is stored in /srv. Make /srv group root and writeable by group members (so that any admin can work on it without sudo), set also the sticky bit, so that new folders there inherit the properties:*

```
#!bash
sudo chown root:sudo /srv
sudo chmod g+ws /srv

```

*Create*

*A subfolder for django projects
*A subfolder for python virtual environments
*A subfolder for db backups
*And project specifict folder for apache configuration files:

```
#!bash
mkdir /srv/django-projects
mkdir /srv/pve
mkdir /srv/dbdumps
sudo mkdir /etc/apache2/django-projects/
sudo mkdir /etc/nginx/django-projects/
```

## Python: Pip & Virtual Environments ##

*Install the:*

```
#!bash
sudo apt-get install python-pip
sudo apt-get install python-virtualenv
```


*Restart the server web servers*

```
#!bash
sudo /etc/init.d/nginx restart
sudo /etc/init.d/apache2 reload
```

## Mail ##

*The most efficient way to install Postfix and other programs needed for testing email is to install the mailutils package by typing:*

```
#!bash
sudo apt-get install mailutils
```
*Select Internet site in the following menu;

![mailsetup.png](https://bitbucket.org/repo/n95Lbe/images/663802870-mailsetup.png)

*Edit the /etc/postfix/main.cf file;

```
#!bash
sudo nano /etc/postfix/main.cf
```
*With the file open, scroll down until you see the entries shown in this code block:*

```
#!bash
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = all
```

*Change the line that reads inet_interfaces = all to inet_interfaces = loopback-only. When you're done, that same section of the file should now read:*

```
#!bash
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = loopback-only
```
# From your local computer #
*Install the required django libraries using [note that you need to be at the root path of the project]:*


```
#!bash
pip install -r requirements.txt
```
*Activate the virtual environment:*


```
#!bash

source [path to virtual environment]/bin/activate
```


# Update the source code to your preferences #

*You need to update the setting.py file for the server links and resources to work properly*

```
#!python

deploymentHosts = ['put here the name of your deployment host' ]

ROOT_URL_PREFIX = 'http://[the IP or URL of the server]/'
```