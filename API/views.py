import numpy as np
from datetime import timedelta
from datetime import datetime
from operator import itemgetter

from logging import getLogger
logger = getLogger('custom')

from django.db.models import Sum
from django.utils import simplejson
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from frontend.models import Computer, Event, UserProfile, User, Session

from django.db import connection
from django.utils.timezone import utc

from django.db.models import Max,Min, Count

"""
timestampdiff is a helper method used to calculate the difference between the current timestamp
and its previous timestamp.
"""
def timestampdiff(array):
    diffArray = [timedelta(minutes=5)]
    for x in range(1, len(array)):
        diffArray.append(array[x]-array[x-1])
    return np.array(diffArray)

         
"""
eventsdiff is a helper method used to identify when an event is different
to the previous event and as such it identifies the end of sessions.
"""
def eventsdiff(array):
    diffArray = [1]
    for x in range(1, len(array)):
        diffArray.append(array[x]-array[x-1])
    return np.array(diffArray)


"""
update_sessions_from_events takes the arguments of a computer(is this neccesary any more?) 
and a queryset of computerEvents.

The computer events are ordered by timestamp to prevent negative length sessions being created.
Sessions for the computer are then identified from the computerEvents and saved to the Session table.
"""
#@transaction.commit_on_success 
def update_sessions_from_events(computer, computerEvents):
    computerEvents = computerEvents.order_by('timestamp')
    events_data = [x for x in computerEvents.values_list('timestamp', 
                                                         'eventType', 
                                                         'bustedBy')]
    if computerEvents.count() < 2:
        return
    
    logger.debug('update_sessions_from_events %s latest timestamp: %s'
                % (computer.owner.user.username, events_data[-1][0]))
    
    lut = {}
    lut['CI', None] = 2
    lut['CA', None] = 1
    lut['CO', None] = 0
    for user in UserProfile.objects.all():
        lut['CI', user.id] = 3
    status_entries = [lut[(x[1], x[2])] for x in events_data]
    #status_entries = [status_entries[0], ] + status_entries
    status_entries_arr = np.array(status_entries)
    d_arr = eventsdiff(status_entries_arr)
    status_index = d_arr != 0
    status_index = [False,] + status_index
    
    timestamps = [x[0] for x in events_data]
    #timestamps = [timestamps[0], ] + timestamps
    timestamps_array = np.array(timestamps)
    diff_timestamps = timestampdiff(timestamps_array)
    timestamps_index = diff_timestamps >= timedelta(minutes=1)
    timestamps_index = [False,] + timestamps_index
    
#    for t in timestamps_array[timestamps_index]:
#        print t.strftime('%d %a %H:%M:%S')
#    print
    
#    from matplotlib import pyplot as plt
#    fig = plt.figure()
#    plt.plot([x for x in timestamps], [x.total_seconds() for x in diff_timestamps], '.')
#    plt.plot([x for x in timestamps], [x*2 for x in status_entries], '-')
#    fig.autofmt_xdate()
#    plt.show()
    
    # filter out sessions that are just 1 event
    for i, ev in enumerate(status_index[1:], 1):
        if ev == True and status_index[i-1] == True:
            status_index[i-1]=False
            status_index[i]=False
            
    to_remove = []
    for i, ev in enumerate(timestamps_index[1:], 1):
        if ev == True and timestamps_index[i-1] == True:
            timestamps_index[i-1]=False
            to_remove.append(i)

#    for t in timestamps_array[timestamps_index]:
#        print t.strftime('%d %a %H:%M:%S')
#    print

    index = np.bitwise_or(status_index, timestamps_index)

    # filter out sessions that are just 1 event
    for i, ev in enumerate(index[1:], 1):
        if ev == True and index[i-1] == True:
            index[i-1] = True
            index[i] = False
            
    # 'close sessions'
    for i, ev in enumerate(index[1:], 1):
        if ev==True:
            if i not in to_remove:
                index[i-1] = True
            else:
                #print 'i in to_remove'
                index[i-2] = True
    
    index[0] = True
    index[-1] = True
    
    try:
        sessions_timestamps = timestamps_array[index]
    except Exception as e:
        extra = ' len(timestamps_array): %d; len(index): %d' % (len(timestamps_array), len(index))
        raise type(e)(e.message + extra)
    
    itr = iter(sessions_timestamps)
    for ev in itr:
        session_start = ev
        start_event = computerEvents.get(timestamp=session_start)
        try:
            session_end = itr.next()
            end_event = computerEvents.get(timestamp=session_end)
        except StopIteration:
            logger.debug('StopIteration')
            end_event = computerEvents.latest('timestamp')
            session_end = end_event.timestamp
        length = round((session_end - session_start).total_seconds())
        if start_event.eventType != end_event.eventType:
            #If this happens, just don't save the session.
            msg = 'start_event.eventType != end_event.eventType: %s: %s, %s: %s' % (
                start_event.timestamp, start_event.eventType, 
                end_event.timestamp, end_event.eventType)
            logger.debug(msg) 
            continue
        newSession = Session(start_event=start_event, length=length, end_event=end_event)
        newSession.save()
        
def update_sessions_from_events2(computer, timestamp_to_start_from=None):
    cursor = connection.cursor()
    if timestamp_to_start_from is None:
        
        sqlquery = ''' SELECT *, cast (EXTRACT( EPOCH FROM sessions."length") AS INT) AS "intLength"
        FROM (SELECT *, "endTimestamp"-"startTimestamp" AS "length"
        FROM (    SELECT     prevnexteventdiffsessions."id" AS "startEventId",
                        prevnexteventdiffsessions."eventType" AS "startEventType",
                        prevnexteventdiffsessions."timestamp" AS "startTimestamp",
                        prevnexteventdiffsessions."forComputer_id" AS "startForComputer_id",
                        prevnexteventdiffsessions."bustedBy_id" AS "bustedBy_id",
        
                        lead(prevnexteventdiffsessions."prevId",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endEventId",
                        lead(prevnexteventdiffsessions."prevEventType",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endEventType",
                        lead(prevnexteventdiffsessions."prevTimestamp",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endTimestamp",
                        lead(prevnexteventdiffsessions."prevForComputer_id",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endForComputer_id",
                        lead(prevnexteventdiffsessions."prevBustedBy_id",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endBustedBy_id"
                FROM (
                    SELECT *
                    FROM
                    (
                        SELECT *, "timestamp" - "prevTimestamp" as "timediff"
                        FROM (     SELECT
                                LAG(frontend_event."id",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevId",
                                LAG(frontend_event."eventType",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevEventType",
                                LAG(frontend_event."timestamp",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevTimestamp",
                                LAG(frontend_event."forComputer_id",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevForComputer_id",
                                LAG(frontend_event."bustedBy_id",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevBustedBy_id", 
        
                                frontend_event."id",
                                frontend_event."eventType", 
                                frontend_event."timestamp",
                                frontend_event."forComputer_id",
                                frontend_event."bustedBy_id"
                                FROM public.frontend_event
                                WHERE frontend_event."forComputer_id"=%s
                        ) AS prevnextevent
                    ) AS prevnexteventdiff
                WHERE "prevEventType" IS NULL OR "prevEventType" != "eventType" OR "timediff" > '1 minutes'::interval OR ("prevBustedBy_id" IS NULL AND "bustedBy_id" IS NOT NULL)) AS prevnexteventdiffsessions) AS sessionswithzeronandnonzero) AS sessions
        WHERE "length" > '0 minutes'::interval'''
        cursor.execute(sqlquery,[computer.id])
    else:
        sqlquery = ''' SELECT *, cast (EXTRACT( EPOCH FROM sessions."length") AS INT) AS "intLength"
        FROM (SELECT *, "endTimestamp"-"startTimestamp" AS "length"
        FROM (    SELECT     prevnexteventdiffsessions."id" AS "startEventId",
                        prevnexteventdiffsessions."eventType" AS "startEventType",
                        prevnexteventdiffsessions."timestamp" AS "startTimestamp",
                        prevnexteventdiffsessions."forComputer_id" AS "startForComputer_id",
                        prevnexteventdiffsessions."bustedBy_id" AS "bustedBy_id",
        
                        lead(prevnexteventdiffsessions."prevId",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endEventId",
                        lead(prevnexteventdiffsessions."prevEventType",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endEventType",
                        lead(prevnexteventdiffsessions."prevTimestamp",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endTimestamp",
                        lead(prevnexteventdiffsessions."prevForComputer_id",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endForComputer_id",
                        lead(prevnexteventdiffsessions."prevBustedBy_id",1) OVER (PARTITION BY prevnexteventdiffsessions."forComputer_id" ORDER BY prevnexteventdiffsessions."timestamp") AS "endBustedBy_id"
                FROM (
                    SELECT *
                    FROM
                    (
                        SELECT *, "timestamp" - "prevTimestamp" as "timediff"
                        FROM (     SELECT
                                LAG(frontend_event."id",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevId",
                                LAG(frontend_event."eventType",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevEventType",
                                LAG(frontend_event."timestamp",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevTimestamp",
                                LAG(frontend_event."forComputer_id",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevForComputer_id",
                                LAG(frontend_event."bustedBy_id",1) OVER (PARTITION BY frontend_event."forComputer_id" ORDER BY frontend_event."timestamp") AS "prevBustedBy_id", 
        
                                frontend_event."id",
                                frontend_event."eventType", 
                                frontend_event."timestamp",
                                frontend_event."forComputer_id",
                                frontend_event."bustedBy_id"
                                FROM public.frontend_event
                                WHERE frontend_event."forComputer_id"=%s and frontend_event."timestamp" >= %s
                        ) AS prevnextevent
                    ) AS prevnexteventdiff
                WHERE "prevEventType" IS NULL OR "prevEventType" != "eventType" OR "timediff" > '1 minutes'::interval OR ("prevBustedBy_id" IS NULL AND "bustedBy_id" IS NOT NULL)) AS prevnexteventdiffsessions) AS sessionswithzeronandnonzero) AS sessions
        WHERE "length" > '0 minutes'::interval'''
        cursor.execute(sqlquery,[computer.id,timestamp_to_start_from])
        #print timestamp_to_start_from
    
    for session in cursor.fetchall():
        newSession = Session(start_event_id=session[0], length=session[11], end_event_id=session[5])
        print session[0]
        newSession.save()
        
        
        
    return
    

"""
update_session takes a computer as an argument and from that computer works out the beginning 
of the last session and then calls update_sessions_from_events on any events that follow the 
last session.
"""
def update_sessions(computer):
    sessions = Session.objects.filter(start_event__forComputer=computer)
    events = Event.objects.filter(forComputer=computer)
    if sessions.count() > 0:
        lastSession = sessions.order_by('-start_timestamp')[0]
        # get the start of the last session and store it in a variable
        update_time = lastSession.start_event.timestamp
        
        # delete the last session
        lastSession.delete()

        # calculate from the start of the last sesssion
        events = events.filter(timestamp__gte=update_time)
        
    update_sessions_from_events(computer, events)
    
    
def update_sessions2(computer):
    sessions = Session.objects.filter(start_event__forComputer=computer)
    #events = Event.objects.filter(forComputer=computer)
    if sessions.count() > 0:
        lastSession = sessions.order_by('-start_timestamp')[0]
        # get the start of the last session and store it in a variable
        update_time = lastSession.start_event.timestamp
        
        # delete the last session
        lastSession.delete()

        # calculate from the start of the last sesssion
        #events = events.filter(timestamp__gte=update_time)
        update_sessions_from_events2(computer,update_time)
    else:
        update_sessions_from_events2(computer)

"""
get_busted_count is used by the leaderboards_view method to produce json that outputs the number of times
each user has busted an idle computer. The json output is in descending order.
"""   
def get_busted_count():
    json = []
    users = {}
    for user in UserProfile.objects.all():
        count = Session.objects.filter(start_event__bustedBy=user).count()
        users[user.user.username] = count
    for key in users.keys():
        user = User.objects.get(username=key)
        userprofile = UserProfile.objects.get(user=user)
        json.append({"name" : user.first_name, 
                 "surname" : user.last_name,
                 'username': user.username,
                 "imgurl": userprofile.image.name,
                 "imgFileName": userprofile.image.name.split('/')[-1],
                 "noOfIdleCompBusted" : users[key]})
    json.sort(key=itemgetter('noOfIdleCompBusted'), reverse=True)
    return json


"""
get_lowest_idle_time is used by the leaderboards_view method to ouput json that shows the
users ratio of idle time to total time. The json is ordered in ascending order.
"""
def get_lowest_idle_time():
    json = []
    for computer in Computer.objects.all():
        sessions = Session.objects.filter(start_event__forComputer=computer)
        idle_sessions = sessions.filter(start_event__eventType="CI", start_event__bustedBy=None)
        #active_sessions = sessions.filter(start_event__eventType="CA")
        try:

            idle_time = 0.0
            try:
                #If the person does not have Idle events make make it return 0.0
                idle_time = idle_sessions.aggregate(Sum('length'))['length__sum'] / 60.0
            except:
                pass;

            total_time=0.0
            try:
                total_time = sessions.aggregate(Sum('length'))['length__sum'] / 60.0
            except:
                pass;

            idle_time_proportion=0.0

            try:
                idle_time_proportion = float(idle_sessions.aggregate(Sum('length'))['length__sum']) / float(sessions.aggregate(Sum('length'))['length__sum'])
            except:
                pass

            idle_time_percentage = idle_time_proportion *100

            json.append({"name" : computer.owner.user.first_name,
                         "surname" : computer.owner.user.last_name,
                         'username': computer.owner.user.username,
                         "imgurl": computer.owner.image.name, 
                         "idleTime" : idle_time,
                         'totalTime': total_time,
                         'idleTimeP'
                         'roportion': idle_time_proportion,
                         "imgFileName": computer.owner.image.name.split('/')[-1],
                         'percentage':idle_time_percentage
                         })
        except:
            pass
    json.sort(key=itemgetter('idleTimeProportion'))
    return json


"""
get busted time is a helper method used by the leaderboard_views method to get json of the users 
with the most time busting other users.
"""
def get_busted_time():
    json = []
    users = {}
    for user in UserProfile.objects.all():
        sessions = Session.objects.filter(start_event__bustedBy=user)
        total = sessions.aggregate(Sum('length'))['length__sum']
        if total is not None:
            users[user.user.username] = total
        else:
            users[user.user.username] = 0
    for key in users.keys():
        user = User.objects.get(username=key)
        userprofile = UserProfile.objects.get(user=user)
        json.append({"name" : user.first_name, 
                 "surname" : user.last_name,
                 'username': user.username,
                 "imgurl": userprofile.image.name,
                 "imgFileName": userprofile.image.name.split('/')[-1],
                 "idleTime" : round(users[key]/60)})
    json.sort(key=itemgetter('idleTime'), reverse=True)
    return json


"""
leaderboards_view returns the leaderboards json for the lowest idle time, highest busted count and 
greatest time busting other users in the system.
"""
def leaderboards_view(request):
    #HEEEEEEEEEERE
    if request.user.is_authenticated():
        userProfile = UserProfile.objects.get(user=request.user)
        logger.info("%s leaderboards_view username: %s." % (str(datetime.now()),userProfile.user.username))
        
    for c in Computer.objects.all():
        update_sessions2(c)
    
    data = {}
    data["lowest_idle_time"] = get_lowest_idle_time()
    data["busted_count"] = get_busted_count()
    data["busted_time"] = get_busted_time()
    
    json = simplejson.dumps(data)
    return HttpResponse(json, mimetype='application/json')


"""
to_dict is a helper method used by the sessions_view to generate a dictionary from a session and 
a bustedBy username.
"""
def to_dict(session, username):
    return {"eventType" : session.start_event.eventType,
            "timestamp" : str(session.start_event.timestamp),
            "bustedBy" : username,
            "length" :  session.length}

def session_to_dict_keeping_naming(session):
    dict={}
    if session.start_event.bustedBy is not None:
        dict = {"start_event": str(session.start_event.timestamp),
            "end_event": str(session.end_event.timestamp),
            "sessionType": session.start_event.eventType,
            "username": session.start_event.forComputer.owner.user.username,
            "is_busted_by": 'True'
            }
    else:
        dict = {"start_event": str(session.start_event.timestamp),
                "end_event": str(session.end_event.timestamp),
                "sessionType": session.start_event.eventType,
                "username": session.start_event.forComputer.owner.user.username,
                "is_busted_by": 'False'}
    return dict


def session_to_dict_keeping_naming_with_additional_id(session):
    dict={}
    id = session.start_event.forComputer.owner.user.username + str(session.start_event.timestamp.year) + "_" + str(session.start_event.timestamp.month)+"_"+str(session.start_event.timestamp.day)

    start_event = datetime(2000,1,1,session.start_event.timestamp.hour, session.start_event.timestamp.minute, session.start_event.timestamp.second)
    end_event = datetime(2000,1,1,session.end_event.timestamp.hour, session.end_event.timestamp.minute, session.end_event.timestamp.second)

    if session.start_event.bustedBy is not None:
        dict = {"start_event": str(start_event),
            "end_event": str(end_event),
            "sessionType": session.start_event.eventType,
            "username": session.start_event.forComputer.owner.user.username,
            "is_busted_by": 'True',
            "id": str(session.start_timestamp)
            }
    else:
        dict = {"start_event": str(start_event),
                "end_event": str(end_event),
                "sessionType": session.start_event.eventType,
                "username": session.start_event.forComputer.owner.user.username,
                "is_busted_by": 'False',
                "id": str(session.start_timestamp)
        }
    return dict
"""
sessions_view takes a userprofile_id and from that gets a computer for which it updates
the sessions before outputting those sessions to json.
"""
def sessions_view(request, userprofile_id):
    computer = get_object_or_404(Computer, owner_id=userprofile_id)

    update_sessions(computer)
    sessions = Session.objects.select_related().filter(start_event__forComputer=computer)
    
    non_busted_sessions = sessions.filter(start_event__bustedBy=None)
    busted_events = sessions.exclude(start_event__bustedBy=None)
    nb_data = [to_dict(x, None) for x in non_busted_sessions.iterator()]
    busted_data = [to_dict(x, x.start_event.bustedBy.user.username) 
                   for x in busted_events.iterator()]
    
    json = simplejson.dumps(nb_data+busted_data)
    
    return HttpResponse(json, mimetype='application/json')

def all_sessions_view(request):
    #TODO update sessions here
    
    sessions=Session.objects.all()
    sessions = [session_to_dict_keeping_naming(x) for x in sessions]
    
    json = simplejson.dumps(sessions)
    return HttpResponse(json, mimetype='application/json')
#******************************
def get_how_many_times_a_user_busted_another_view(request):
    #TODO update sessions here

    busted_sessions = Session.objects.filter(
                        start_event__eventType="CI").exclude(start_event__bustedBy__isnull=True)\
                        .exclude(start_event__bustedBy__user__username='mario')\
                        .exclude(start_event__bustedBy__user__username='sonic')\
                        .exclude(start_event__bustedBy__user__username='et2e10')\
                        .exclude(start_event__forComputer__owner__user__username='mario')\
                        .exclude(start_event__forComputer__owner__user__username='sonic')\
                        .exclude(start_event__forComputer__owner__user__username='et2e10')\
                        .values('start_event__bustedBy__user__username','start_event__forComputer__owner__user__username').annotate(bust_count= Count('start_event__bustedBy__user__username'))


    #Player.objects.values('player_type', 'team').order_by().annotate(Count('player_type'), Count('team'))

    debug = busted_sessions.query
    results = []
    for how_many_times_x_busted_y in busted_sessions:
        dict = {"source": str(how_many_times_x_busted_y['start_event__bustedBy__user__username']),
            "target": str(how_many_times_x_busted_y['start_event__forComputer__owner__user__username']),
            "type": how_many_times_x_busted_y['bust_count'],
            }
        results.append(dict)


    #sessions = [session_to_dict_keeping_naming(x) for x in sessions]

    json = simplejson.dumps(results)
    return HttpResponse(json, mimetype='application/json')
#&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
def get_sessions_of_user_view(request, username):
    #TODO update sessions here
    sessions = Session.objects\
                        .filter(start_event__forComputer__owner__user__username=username).filter(end_timestamp__lt=datetime(2014,3,7,17,0))


    sessions = [session_to_dict_keeping_naming_with_additional_id(x) for x in sessions]

    json = simplejson.dumps(sessions)
    return HttpResponse(json, mimetype='application/json')



def sessions_metadata_view(request):
    #TODO update sessions here
    
    metadata={"min_date": str(Session.objects.all().aggregate(Min('start_timestamp'))["start_timestamp__min"]),
     "max_date": str(Session.objects.all().aggregate(Max('end_timestamp'))["end_timestamp__max"]),
    }
    
    json = simplejson.dumps(metadata)
    return HttpResponse(json, mimetype='application/json')

def sessions_filtered_view(request, start_date=datetime(2013,12,10,9,0),end_date=datetime(2013,12,10,20,0)):
    #TODO update sessions here
    
    sessions=Session.objects.filter(start_timestamp__gte=start_date).filter(end_timestamp__lte=end_date);
    sessions = [session_to_dict_keeping_naming(x) for x in sessions]
    
    json = simplejson.dumps(sessions)
    return HttpResponse(json, mimetype='application/json')


def test_sessions_view(request, userprofile_id):
    computer = get_object_or_404(Computer, owner_id=userprofile_id)

    update_sessions2(computer)
    sessions = Session.objects.select_related().filter(start_event__forComputer=computer)
    
    non_busted_sessions = sessions.filter(start_event__bustedBy=None)
    busted_events = sessions.exclude(start_event__bustedBy=None)
    nb_data = [to_dict(x, None) for x in non_busted_sessions.iterator()]
    busted_data = [to_dict(x, x.end_event.bustedBy.user.username) 
                   for x in busted_events.iterator()]
    
    json = simplejson.dumps(nb_data+busted_data)
    
    return HttpResponse(json, mimetype='application/json')

"""
get_no_of_computers_that_are_CA TODO
"""
def get_no_of_computers_that_are_CA_view(request, start_date=datetime(2013,12,10,9,0),end_date=datetime(2013,12,10,20,0)):
    #TODO: We need to implement update sessions that apply to all computers not just to this a specific one.
    client_date_time_format = '%Y-%m-%d %H:%M:%S.%f'
    
    start=clientDateTimeDateTime=datetime.strptime('2013-12-10 09:00:00.810844', client_date_time_format).replace(tzinfo=utc)
    end=clientDateTimeDateTime=datetime.strptime('2013-12-10 21:00:00.810844', client_date_time_format).replace(tzinfo=utc)
    sessions = Session.objects.filter(start_timestamp__gte=start).filter(end_timestamp__lte=end)
    
    
    
    print 'ok'
    return HttpResponse('', mimetype='application/json')
    