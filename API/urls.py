from django.conf.urls import patterns
from django.conf import settings
from API.views import leaderboards_view, sessions_view, test_sessions_view,get_no_of_computers_that_are_CA_view, all_sessions_view, sessions_filtered_view,sessions_metadata_view, get_how_many_times_a_user_busted_another_view, get_sessions_of_user_view
from django.conf.urls.static import static


urlpatterns = patterns('',

    (r'^userstats/leaderboards$', leaderboards_view),
    (r'^userstats/sessions$', all_sessions_view),
    (r'^userstats/sessions/metadata$', sessions_metadata_view),
    (r'^userstats/sessionsfiltered/(?P<start_date>[-:\w]+)/(?P<end_date>[-:\w]+)$', sessions_filtered_view),
    (r'^userstats/timesauserbustedanother$', get_how_many_times_a_user_busted_another_view),
    (r'^userstats/getusersessions/(?P<username>[-\w\ ]+)$', get_sessions_of_user_view),

    (r'^userstats/events/getnoofcomputers$', get_no_of_computers_that_are_CA_view),
    (r'^userstats/events/(?P<userprofile_id>[-\w]+)$', test_sessions_view),
    (r'^userstats/events/test/(?P<userprofile_id>[-\w]+)$', test_sessions_view),

    
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)