from datetime import datetime
from logging import getLogger
logger = getLogger('custom')

from django.db import transaction
from django.http import HttpResponse
from django.utils.timezone import utc
from django.template import RequestContext
from django.template.context import Context
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.views import login
from django.contrib.sites.models import Site, RequestSite

from basicutils.djutils import get_json_success, get_json_success_with_hash, get_requested_user, get_json_info, get_json_success_and_busted_info, get_json_authentication_success

from .decorators import access_required, http_basic_auth, admin_access_required

from frontend.forms import UserRegistrationForm
from frontend.models import Computer, Event, UserProfile, BustProfile

from registration import signals
from registration.models import RegistrationProfile
from registration.backends.default.views import RegistrationView

from frontend.context_processors import default_context

from django.conf import settings

from API.views import get_lowest_idle_time, get_busted_count, get_busted_time
from PIL import Image
import glob, os

"""
home_view takes a request and returns the html rendering of the main front page of the IdleWars Website.
Found at: root_url
"""
def home_view(request):
    if request.user.is_authenticated():
        userProfile = UserProfile.objects.get(user=request.user)
        logger.info("%s home_view username: %s." % (str(datetime.now()),userProfile.user.username))
    return login(request, template_name='mainPageDesktop.html', redirect_field_name='')



"""
leaderboards_screen_view returns the leaderboard to be displayed on the big screen.
Found at: root_url/leaderboards_screen/
"""
def leaderboards_screen_view(request):
    if request.user.is_authenticated():
        userProfile = UserProfile.objects.get(user=request.user)
        logger.info("leaderboards_screen_view username %s." % (userProfile.user.username))
    context = default_context(request)
    return render(request, 'leaderboardScreen.html', context)

"""
leaderboards_screen_view returns the leaderboard to be displayed on the big screen.
Found at: root_url/leaderboards_screen/
"""
def leaderboards_screen_view2(request):
    if request.user.is_authenticated():
        userProfile = UserProfile.objects.get(user=request.user)
        logger.info("leaderboards_screen_view username %s." % (userProfile.user.username))

    context = default_context(request)

    context.update({"busted_time":get_busted_time()})
    context.update({"busted_count":get_busted_count()})
    context.update({"lowest_idle_time":get_lowest_idle_time()})


    return render(request, 'leaderboardScreen2.html', context)

def leaderbards_screen_view_iframe(request):
    context = default_context(request)
    return render(request, 'leaderboardScreenIframe.html', context)


"""
timeline_view is part of the debugging section of the website.
It takes a request from which it deduces the user who made the request and then
returns the correct bar visualisation for that user.
Found at: root_url/debug
"""
@access_required
def timeline_view(request):
    userProfile = UserProfile.objects.get(user=request.user)
    context = RequestContext(request, {'userProfile' : userProfile})
    return render(request, 'userBarDesktop.html', context)



"""
all_timelines_view is part of the debugging section of the website.
It returns a html rendering of the bar visualisations for all the users in the IdleWars study.
Found at: root_url/debug/all_bars
"""
@access_required        
def all_timelines_view(request):
    context = RequestContext(request)
    return render(request, 'debug_board.html', context)

@admin_access_required
def gantt_chart_view(request):
    context = RequestContext(request)
    return render(request, 'gatt_chart.html', context)
@admin_access_required
def gantt_chart_view2(request):
    context = RequestContext(request)
    return render(request, 'gatt_chart2.html', context)
@admin_access_required
def graph_view(request):
    context = RequestContext(request)
    return render(request, 'graph2.html', context)
def show_installation_instructions(request):
    #context = RequestContext(request)
    context=default_context(request)
    return render(request, 'installation_instructions.html', context)
def show_play_instructions(request):
    context = default_context(request)
    return render(request,'play_instructions.html', context)

"""
TODO: Ask Vangelis if this is used, it doesnt seem to be :/
"""
@http_basic_auth
def verify_user(request):
    res=HttpResponse(get_json_authentication_success(),status=201)
    res["Content-Type"]="application/json"; 
    return res


"""
UserProfileRegistrationClass is a subclass of RegistrationView from the djang-registration module.
The class has a method register that first creates a User in the User table using the RegistrationView.
Then register will create a UserProfile and Computer in the respective tables for the created user.
Found at: Found at: root_url/accounts/register/
"""
class UserProfileRegistrationClass(RegistrationView):
    form_class = UserRegistrationForm
                
    def register(self, request, **cleaned_data):
            username, email, password, user_image, first_name, last_name, computer_type = cleaned_data['username'], cleaned_data['email'], cleaned_data['password1'], cleaned_data['user_image'], cleaned_data['first_name'], cleaned_data['last_name'], cleaned_data['computer_type']
            if Site._meta.installed:
                site = Site.objects.get_current()
            else:
                site = RequestSite(request)
            new_user = RegistrationProfile.objects.create_inactive_user(username, email,
                                                                        password, site, True)
            
            new_user.first_name = first_name
            new_user.last_name = last_name
            with transaction.commit_on_success():
                new_user.save()
                
                signals.user_registered.send(sender=self.__class__,
                                             user=new_user,
                                             request=request)
                
                usrProf = UserProfile(user=new_user, image=user_image)
                usrProf.save()
                BustProfile.objects.update_profile(usrProf)
    
                logger.info("User %s created and saved" % (username))
                
                comp = Computer(name = username, url = "http://%s.%s.co.uk" % (username, username), owner = usrProf, typeOfComputer=computer_type)
                comp.save()

                size = 100, 100
                im = Image.open(usrProf.image.path)
                im.thumbnail(size, Image.ANTIALIAS)
                im.save(settings.MEDIA_ROOT+'profileImg/sml_'+user_image.name.split('/')[-1], "JPEG")

                
                #TODO: Create a for loop with this.
                """idlp= Event(forComputer=comp,
                    eventType="CA",
                    macAddress="FF:FF:FF:FF:FF:FF",
                    appver="0.6",
                    os="mac os 10",
                    ipAddress="255.255.255.255",
                    clientTimestamp="2013-11-11 14:02:00.734",
                    timestamp=      "2013-11-11 14:02:00.734")
                idlp.save()
                idlp= Event(forComputer=comp,
                    eventType="CA",
                    macAddress="FF:FF:FF:FF:FF:FF",
                    appver="0.6",
                    os="mac os 10",
                    ipAddress="255.255.255.255",
                    clientTimestamp="2013-11-11 14:02:02.734",
                    timestamp=      "2013-11-11 14:02:02.734")
                idlp.save()
                idlp= Event(forComputer=comp,
                        eventType="CA",
                        macAddress="FF:FF:FF:FF:FF:FF",
                        appver="0.6",
                        os="mac os 10",
                        ipAddress="255.255.255.255",
                        clientTimestamp="2013-11-11 14:02:04.734",
                        timestamp=      "2013-11-11 14:02:04.734")
                idlp.save()
                idlp= Event(forComputer=comp,
                        eventType="CA",
                        macAddress="FF:FF:FF:FF:FF:FF",
                        appver="0.6",
                        os="mac os 10",
                        ipAddress="255.255.255.255",
                        clientTimestamp="2013-11-11 14:02:06.734",
                        timestamp=      "2013-11-11 14:02:06.734")
                idlp.save()


                idlp= Event(forComputer=comp,
                        eventType="CA",
                        macAddress="FF:FF:FF:FF:FF:FF",
                        appver="0.6",
                        os="mac os 10",
                        ipAddress="255.255.255.255",
                        clientTimestamp="2013-11-11 14:02:08.734",
                        timestamp=      "2013-11-11 14:02:08.734")
                idlp.save()

                idlp= Event(forComputer=comp,
                        eventType="CA",
                        macAddress="FF:FF:FF:FF:FF:FF",
                        appver="0.6",
                        os="mac os 10",
                        ipAddress="255.255.255.255",
                        clientTimestamp="2013-11-11 14:02:10.734",
                        timestamp=      "2013-11-11 14:02:10.734")
                idlp.save()

                idlp= Event(forComputer=comp,
                        eventType="CA",
                        macAddress="FF:FF:FF:FF:FF:FF",
                        appver="0.6",
                        os="mac os 10",
                        ipAddress="255.255.255.255",
                        clientTimestamp="2013-11-11 14:02:12.734",
                        timestamp=      "2013-11-11 14:02:12.734")
                idlp.save()

                idlp= Event(forComputer=comp,
                        eventType="CA",
                        macAddress="FF:FF:FF:FF:FF:FF",
                        appver="0.6",
                        os="mac os 10",
                        ipAddress="255.255.255.255",
                        clientTimestamp="2013-11-11 14:02:14.734",
                        timestamp=      "2013-11-11 14:02:14.734")
                idlp.save()


                idlp= Event(forComputer=comp,
                        eventType="CI",
                        macAddress="FF:FF:FF:FF:FF:FF",
                        appver="0.6",
                        os="mac os 10",
                        ipAddress="255.255.255.255",
                        clientTimestamp="2013-11-11 14:02:16.734",
                        timestamp=      "2013-11-11 14:02:16.734")
                idlp.save()

                idlp= Event(forComputer=comp,
                        eventType="CI",
                        macAddress="FF:FF:FF:FF:FF:FF",
                        appver="0.6",
                        os="mac os 10",
                        ipAddress="255.255.255.255",
                        clientTimestamp="2013-11-11 14:02:17.734",
                        timestamp=      "2013-11-11 14:02:17.734")
                idlp.save()

                idlp= Event(forComputer=comp,
                        eventType="CA",
                        macAddress="FF:FF:FF:FF:FF:FF",
                        appver="0.6",
                        os="mac os 10",
                        ipAddress="255.255.255.255",
                        clientTimestamp="2013-11-11 14:02:19.734",
                        timestamp=      "2013-11-11 14:02:19.734")
                idlp.save()"""
                
 #HHHHHEEEERRRREEE                      
            return new_user


"""
create_event takes a request and a computer name. 
From the request, the method works out if the application being used is the correct version.
If the correct version is being used, the computer deduced from the computer name is checked to see if it
is busted, active, or idle before the event is saved.
If an incorrect version is being used the event is just ignored with no error thrown.
TODO: Does it actually ignore the event? I see no code corresponding to that.
Found at: root_url/rawinput/<com_name>/events/ (where com_name is a computer name in the computer table)
"""
@require_POST
@csrf_exempt
def create_event2(request, com_name):
    
    #TODO: Does this try except do anything? :S :S:S
    try:
        version = request.POST['appver']
        if version not in ('0.2', '0.3', '0.4', '0.5','0.6'):
            res=HttpResponse(status=201)
            res["Content-Type"]="application/json";
    except:
        res=HttpResponse(status=201)
        res["Content-Type"]="application/json";
        
        return res
        
    eventType=request.POST["eventType"]
    mac= request.POST["mac"]
    appver= request.POST["appver"]
    os = request.POST["os"]
    
    try:
        clientDateTimeFormat = '%Y-%m-%d %H:%M:%S.%f'
        clientTimestamp = request.POST["clientTimestamp"]
        clientDateTimeDateTime=datetime.strptime(clientTimestamp, clientDateTimeFormat).replace(tzinfo=utc)
    except:
        clientDateTimeDateTime = None
    ip=""
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    
    #if the application is not the latest ignore it without indicating an error.
    
    
    comp=Computer.objects.get(name=com_name)
    
    #eventsOfComputer = Event.objects.filter(forComputer=comp).order_by("timestamp") #.len()
    #noOfEvents = eventsOfComputer.count()
    #If computer does not have events
    #if noOfEvents != 0:
    #Get the last event
    #lastEventOfComputer = eventsOfComputer.latest(field_name="timestamp")
    #if the event is of type computer idle and it was busted by a person
    #lastEventOfComputer.bustedBy
    #TODO: replace lastEventOfComputer.bustedBy with compp.currentBystedBy
    #if the current event is CI and the previous status has busted
    if (eventType=="CI") and (comp.currentBustedBy) and (comp.currentEventType=="CI"):
        
        #lastEventOfComputer.eventType=="CI"
        #Create a new CI event that has busted by they value of the previous busted by  
        
        #TODO: replace lastEventOfComputer.bustedBy with compp.currentBystedBy
        idlp= Event(forComputer=comp,
                    eventType=eventType,
                    bustedBy=comp.currentBustedBy,
                    macAddress=mac,
                    appver=appver,
                    os=os,
                    ipAddress=ip,
                    clientTimestamp=clientDateTimeDateTime)
        
        
        idlp.save()
            
        res=HttpResponse(get_json_success_and_busted_info(idlp.id,bbName=idlp.bustedBy.user.first_name, bbSurname=idlp.bustedBy.user.last_name,imgURL=idlp.bustedBy.image.url.replace("https://","http://")),status=201)
        res["Content-Location"]="/computer/" + comp.name+"/%s/" % (idlp.id)
        res["Content-Type"]="application/json";
        #print "COMPUTER BUSTED"
        
        return res

   
    #TODO: The following two saves need to be ATOMIC (use Django Atomic transactions)
    #print "Computer not busted"
    idlp = Event(forComputer=comp,
                 eventType=eventType,
                 macAddress=mac,
                 appver=appver,
                 os=os,
                 ipAddress=ip,
                 clientTimestamp=clientDateTimeDateTime)
    comp.currentEventType=eventType
    comp.currentBustedBy=None
    with transaction.commit_on_success():
        idlp.save()
        comp.save()
    
    
    #TODO: comp.currentBystedBy=NULL
    #TODO: comp.save()
    #logger.info("Timestamp: %s. EventType: %s. forComputer: %s. MAC: %s. IP: %s" % (idlp.timestamp, idlp.eventType, idlp.forComputer.name, mac, ip))
    #print "saved idlp.eventType: %s" % (idlp.eventType)
    #print "saved idlp.timestamp %s" % (idlp.timestamp)
    debug = BustProfile.objects.get(userprofile__id=comp.id).bust_hash
    res=HttpResponse(get_json_success_with_hash(idlp.id, BustProfile.objects.get(userprofile__id=comp.id).bust_hash),status=201)
    res["Content-Location"]="/computer/" + comp.name+"/%s/" % (idlp.id)
    res["Content-Type"]="application/json";
    return res

        
"""
bust_computer takes a request and a bust_hash.
The bust hash, taken from the url, is used to identify a bustProfile in the BustProfile table, if one is not
found a 404 page is returned. If a bustProfile is returned, the method checks if the computer being busted
is idle, and if so, busts it before generating a new bustprofile for the busted computer. If the bustprofile
is not idle, then an error page is returned.
Found at: root_url/bust/<bust_hash> (where bust_hash is [A-Za-z0-9]{6})
"""
@access_required
@csrf_exempt
def bust_computer2 (request, bust_hash):
    if request.method == "POST" or request.method == "GET":
        bustProfile = get_object_or_404(BustProfile, bust_hash=bust_hash)
        comp = Computer.objects.get(owner=bustProfile.userprofile)
        owner = get_requested_user(request)
        #UserProfile.objects.filter(computer_set__name='comp4')
        
        #event =Event.objects.filter(forComputer=comp).latest(field_name="timestamp")
        logger.info("Bust attempt by: %s. For Computer: %s." % (owner, comp.name))
        #TODO: check if the computer does not have event yet
        #TODO: check the previous idle event if it is busted by the user
        #last_event =com_events.latest(timestamp) 
        
        #Check if the computer is idle
        if comp.currentEventType == "CI":
            
            #TODO: replace event.bustedBy with comp.currentBystedBy
            #check if it is already busted.
            if comp.currentBustedBy:
                logger.info("Bust unsuccessful (the user has already being busted)")
                if request.method == "POST":
                    res=HttpResponse(get_json_info("This computer has been already busted by: %s,  %s ,  %s" % (comp.currentBustedBy.user.username,comp.currentBustedBy.user.last_name, comp.currentBustedBy.user.first_name) ) ,status=202)
                    res["Content-Type"]="application/json";
                else:
                    variables =Context({'header_title': 'Bust', 'content_title':"This computer has been already busted by: ", 'content_description': "Username: %s, Last name: %s , First Name %s" % (comp.currentBustedBy.user.username,comp.currentBustedBy.user.last_name, comp.currentBustedBy.user.first_name)})
                    output = render(request, 'dialogMobile.html', variables)
                    res=HttpResponse(output, status=202)
            else:
                logger.info("Bust successful")
                #TODO: update bust by here.
                owner = get_requested_user(request)
                computerOwner= comp.owner
                if owner.username != computerOwner.user.username:
                    
                    upOwner= UserProfile.objects.get(user=owner)
                    #TODO: make the following atomic
                    
                    comp.currentBustedBy=upOwner
                    #sid = transaction.savepoint()
                    comp.save()
                    BustProfile.objects.update_profile(comp.owner)
                    
                    #TODO: comp.currentBystedBy=upOwner
                    #TODO: comp.save()
                    #transaction.commit()
                    if request.method == "POST":
                        res=HttpResponse(get_json_success(comp.id) ,status=201)
                        res["Content-Location"]="/event/%s/" % (comp.id)
                        res["Content-Type"]="application/json";
                    else:
                        variables =Context({'header_title': 'Bust', 'content_title':"Congratulations!!! ", 'content_description': "You have successfully busted the computer of %s" % (computerOwner.user.username)})
                        output = render(request, 'dialogMobile.html', variables)
                        res=HttpResponse(output, status=201)
                else:
                    logger.info("Bust unsuccessful (User tried to bust himself)")
                    if request.method == "POST":
                        res=HttpResponse(get_json_info("Are you trying to bust yourself?" ) ,status=202)
                        res["Content-Type"]="application/json";
                    else:
                        variables =Context({'header_title': 'Bust', 'content_title':'Weird', 'content_description': 'Are you trying to bust yourself?' })
                        output = render(request, 'dialogMobile.html', variables)
                        res=HttpResponse(output, status=202)
        else:
            logger.info("Bust unsuccessful (the computer is not idle)")
            if request.method == "POST":
                res=HttpResponse(get_json_info("This computer is not idle" ) ,status=202)
                res["Content-Type"]="application/json";
            else:
                variables =Context({'header_title': 'Bust', 'content_title':'Busting attempt failed! ', 'content_description': 'This computer is not idle'})
                output = render(request, 'dialogMobile.html', variables)
                res=HttpResponse(output, status=202)

    return res

@access_required
def bust_by_code(request):
    variables=Context({'header_title': 'Bust', 'content_title':"This computer has been already busted by: "})
    output = render(request, 'bustByCode.html', variables)
    res=HttpResponse(output, status=202)
    return res

"""
@access_required
@csrf_exempt
#@transaction.commit_manually
def bust_computer (request, bust_hash):
    if request.method == "POST" or request.method == "GET":
        bustProfile = get_object_or_404(BustProfile, bust_hash=bust_hash)
        comp = Computer.objects.get(owner=bustProfile.userprofile)
        owner = get_requested_user(request)
        #UserProfile.objects.filter(computer_set__name='comp4')
        
        event =Event.objects.filter(forComputer=comp).latest(field_name="timestamp")
        logger.info("Bust attempt by: %s. For Computer: %s. At: %s" % (owner, comp.name, event.timestamp))
        #TODO: check if the computer does not have event yet
        #TODO: check the previous idle event if it is busted by the user
        #last_event =com_events.latest(timestamp) 
        if event:
            #Check if the computer is idle
            if event.eventType == "CI":
                
                #TODO: replace event.bustedBy with comp.currentBystedBy
                #check if it is already busted.
                if event.bustedBy:
                    logger.info("Bust unsuccessful")
                    if request.method == "POST":
                        res=HttpResponse(get_json_info("This computer has been already busted by: %s,  %s ,  %s" % (event.bustedBy.user.username,event.bustedBy.user.last_name, event.bustedBy.user.first_name) ) ,status=202)
                        res["Content-Type"]="application/json";
                    else:
                        variables =Context({'header_title': 'User Creation', 'content_title':"This computer has been already busted by: ", 'content_description': "Username: %s, Last name: %s , First Name %s" % (event.bustedBy.user.username,event.bustedBy.user.last_name, event.bustedBy.user.first_name)})
                        output = render(request, 'dialogMobile.html', variables)
                        res=HttpResponse(output, status=202)
                else:
                    logger.info("Bust successful")
                    #TODO: update bust by here.
                    owner = get_requested_user(request)
                    computerOwner= event.forComputer.owner
                    if owner.username != computerOwner.user.username:
                        
                        upOwner= UserProfile.objects.get(user=owner)
                        #TODO: make the following atomic
                        
                        event.bustedBy=upOwner
                        #sid = transaction.savepoint()
                        event.save()
                        BustProfile.objects.update_profile(comp.owner)
                        
                        #TODO: comp.currentBystedBy=upOwner
                        #TODO: comp.save()
                        #transaction.commit()
                        if request.method == "POST":
                            res=HttpResponse(get_json_success(event.id) ,status=201)
                            res["Content-Location"]="/event/%s/" % (event.id)
                            res["Content-Type"]="application/json";
                        else:
                            variables =Context({'header_title': 'User Creation', 'content_title':"Congratulations!!! ", 'content_description': "You have successfully busted the computer of %s" % (computerOwner.user.username)})
                            output = render(request, 'dialogMobile.html', variables)
                            res=HttpResponse(output, status=201)
                    else:
                        logger.info("Bust unsuccessful")
                        if request.method == "POST":
                            res=HttpResponse(get_json_info("Are you trying to bust yourself?" ) ,status=202)
                            res["Content-Type"]="application/json";
                        else:
                            variables =Context({'header_title': 'User Creation', 'content_title':'Weird', 'content_description': 'Are you trying to bust yourself?' })
                            output = render(request, 'dialogMobile.html', variables)
                            res=HttpResponse(output, status=202)
            else:
                logger.info("Bust unsuccessful")
                if request.method == "POST":
                    res=HttpResponse(get_json_info("This computer is not idle" ) ,status=202)
                    res["Content-Type"]="application/json";
                else:
                    variables =Context({'header_title': 'User Creation', 'content_title':'Busting attempt failed! ', 'content_description': 'This computer is not idle'})
                    output = render(request, 'dialogMobile.html', variables)
                    res=HttpResponse(output, status=202)

    return res

@require_POST
@csrf_exempt
def create_event(request, com_name):
    try:
        version = request.POST['appver']
        if version not in ('0.2', '0.3'):
            res=HttpResponse(status=201)
            res["Content-Type"]="application/json";
    except:
        res=HttpResponse(status=201)
        res["Content-Type"]="application/json";
        
        return res
        
    
    
    eventType=request.POST["eventType"]
    mac= request.POST["mac"]
    appver= request.POST["appver"]
    os = request.POST["os"]
    
    try:
        clientDateTimeFormat = '%Y-%m-%d %H:%M:%S.%f'
        clientTimestamp = request.POST["clientTimestamp"]
        clientDateTimeDateTime=datetime.strptime(clientTimestamp, clientDateTimeFormat).replace(tzinfo=utc)
    except:
        clientDateTimeDateTime = None
    ip=""
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    
    #if the application is not the latest ignore it without indicating an error.
    
    
    comp=Computer.objects.get(name=com_name)
    
    eventsOfComputer = Event.objects.filter(forComputer=comp).order_by("timestamp") #.len()
    noOfEvents = eventsOfComputer.count()
    #If computer does not have events
    if noOfEvents != 0:
        #Get the last event
        lastEventOfComputer = eventsOfComputer.latest(field_name="timestamp")
        #if the event is of type computer idle and it was busted by a person
        lastEventOfComputer.bustedBy
        #TODO: replace lastEventOfComputer.bustedBy with compp.currentBystedBy
        if (eventType=="CI") and (lastEventOfComputer.bustedBy):
            
            #lastEventOfComputer.eventType=="CI"
            #Create a new CI event that has busted by they value of the previous busted by  
            
            #TODO: replace lastEventOfComputer.bustedBy with compp.currentBystedBy
            idlp= Event(forComputer=comp,
                        eventType=eventType,
                        bustedBy=lastEventOfComputer.bustedBy,
                        macAddress=mac,
                        appver=appver,
                        os=os,
                        ipAddress=ip,
                        clientTimestamp=clientDateTimeDateTime)
            idlp.save()
            
            #print "saved idlp.eventType: %s" % (idlp.eventType)
            #print "saved idlp.timestamp %s" % (idlp.timestamp)
            
            res=HttpResponse(get_json_success_and_busted_info(idlp.id,bbName=idlp.bustedBy.user.first_name, bbSurname=idlp.bustedBy.user.last_name,imgURL=idlp.bustedBy.image.url.replace("https://","http://")),status=201)
            res["Content-Location"]="/computer/" + comp.name+"/%s/" % (idlp.id)
            res["Content-Type"]="application/json";
            #print "COMPUTER BUSTED"
            
            return res
"""