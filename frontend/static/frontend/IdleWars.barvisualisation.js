var IdleWars;
if (!IdleWars) {
    IdleWars = {};
}

IdleWars.barvisualisation = (function() {


	var draw;

	/*Method used to draw the bar visualisation
	Data refers to a json object containing the number of CA, CI, CO and CBusted events
	Total events is the total number of events in the data array
	id is the id of the user to draw the visualisation for*/	
	draw = function(data, totalevents, id, first_date, last_date) {
		
		/* VARIABLES and FUNCTIONS */
		var	
				
				width,
				height,
				padding,
				barwidth,
				duration,
				eventTypes,
				eventTypesCS,
				scale,
				dateScale,
				dateWidthScale,
				color,
				tooltip,
				svgContainer,
				hoursAxis,
				dateAxis,
				xAxisGroup,
				legend,
				
				
				//functions
				chronologicalView, 		//Chronological View is the default view for the bar visualisation
				separatedView,		//Seperated View is the view where the bars are vertically stacked by event type
				groupedView,			//Grouped View is the view where the bars are horizontal by event type
				getValueForType,		//Helper method that returns a value depending on the data object's event type.
				displayToolTip;			//Displays a tooltip when a bar section is hovered over.
		
		/* 
		 * INSTANTIATING VARIABLES 
		 * 
		 * In this section, all the variables defined above are instantiated with their initial values.
		 * 
		 * */
			
		width = $(".practice_container").width();
		height = $(".practice_container").height()-$(".form_container").height();
		padding = 30;	
		barwidth = 75;
		duration = 500;
		eventTypes = ['Computer Active', 'Computer Idle', 'Computer Busted'];
		eventTypesCS = ['green', 'red', 'orange'];
			
		//Scales the data values so that they fit within the width.
		scale = d3.scale.linear()
				.domain([0, totalevents])
				.range([0, width-padding]);
					
		//Returns an x coordinate depending on the timestamp of the data, used for the chronological view		
		dateScale = d3.time.scale()
					.domain([first_date, last_date])
					.range([0, width-padding]);
			
		//Returns a width depending on the length of a segement - used for the chronological view
		dateWidthScale = d3.scale.linear()
						.domain([0, (last_date-first_date)/1000])
						.range([0, width-padding]);
			
		//Gives a colour scale to switch on the different type of events.
		//CA = green, CI = red, CBusted = orange
		color = d3.scale.ordinal().domain(eventTypes)
				.range(eventTypesCS);

		//Default settings for the tooltip that shows event information when the bar is hovered on
		tooltip = d3.select("body")
					.append("div")
					.attr("class", "tooltip")
					.style("opacity", 0);
			
		//The container for the bar chart with the user id.
		svgContainer = d3.select("#bar" + id).append("svg:svg").attr("width", width);

		//Hours axis is used for the separated and grouped views to display the cumulative time in hours/minutes/seconds.
		hoursAxis = d3.svg.axis()
					.scale(scale)
					.tickFormat(function(d){ 
						var hours = Math.floor(d / 3600);
						var time = d - hours * 3600 ;
						var minutes = Math.floor(time / 60);
						var seconds = time - minutes * 60;
						if (totalevents < 3600) {
							return minutes + "m" + seconds+ "s";
						}
						else if (totalevents < (3600 * 12)) {
							return hours + "h" + minutes + "m";
						}
						else {
							return hours + "h";
						}
								
					});
		
		//DateAxis is used in the chronological view to display the dates on the axis						
		dateAxis = d3.svg
					.axis()
					.scale(dateScale)
					.tickFormat(function(d){ 
						return d3.time.format('%d %b %H:%M')(d);
					});

		//Create an SVG group Element for the Axis elements
		xAxisGroup = svgContainer.append("g")
							.attr("class", "axis")
							.attr("transform", "translate(" + padding/2 + "," + barwidth + ")");
							
		//Legend to display the different event types and their respective colours
		legend = svgContainer.append("g")
						.attr("class", "legend")
						.attr("height", 15)
						.attr("width", eventTypes.length*100)
						.attr("transform", "translate(" + padding/2 + ", " + (barwidth + 25) + ")");

		

		/* 
		 * SETUP VARIABLES
		 * 
		 * The Variables instantiated above are now operated upon to setup the bar visualisation.
		 * This includes creating the bar sections on the svgcontainer.
		 * 
		 *  */

		//Creates the sections on the bar, colours and attaches mouse listeners.
		svgContainer.selectAll("rect")
					.data(data)
					.enter()
					.append("svg:rect")
					.attr("class", "barsection")
					.attr("height", barwidth)
					.attr("fill", function(d) {
						return color(d.eventType);
					})
					.on("mouseover", function(d) {
						displayToolTip(d);
					})
					.on("mousemove", function(d) {
							displayToolTip(d);
					})
					.on("mouseout", function(d) {
							tooltip.transition().duration(duration).style("opacity", 0);
					});
			
		//Creates a colour key for the legend						
		legend.selectAll("rect")
				.data(eventTypes)
				.enter()
				.append("rect")
				.attr("x", function (d, i) {
					return i*110;
				})
				.attr("y", 0)
				.attr("width", 10)
				.attr("height", 10)
				.style("fill", function(d) {
					return color(d);
				});
				
		//Gives each key in the legend a label.	
		legend.selectAll("text")
				.data(eventTypes)
				.enter()
				.append("text")
				.attr("x", function(d, i) {
					return i*110 + 12;
				})
				.attr("y", 10)
				.text(function(d) {
					return d;
				});
			
		/* 
		 * FUNCTION DECLARATIONS 
		 * 
		 * The functions defined at the top of this closure are defined in this section.
		 * 
		 * */		
								
		//Method called when the chronological radio button is selected
		chronologicalView = function() {
				
			//Ensures the axis displayed is the dateAxis	
			xAxisGroup.call(dateAxis);
				
			//Moves the legend to below the axis (needed incase separated view was used and as such the axis has moved down)
			legend.transition().delay(duration)
					.duration(duration)
					.attr("transform", "translate(" + padding/2 + "," + (barwidth + 25) + ")");
						
			//Moves the axis to below the bar (needed incase separated view was used and as such the axis has moved down)
			xAxisGroup.transition().delay(duration)
				.duration(duration).attr("transform", "translate(" + padding/2 + "," + barwidth + ")");
			
			//Selects all the bar sections and orders them chronologically before positioning them and giving them the correct width	
			svgContainer.selectAll("#bar" + id + " " + ".barsection")
				.sort(function (a, b) {
						if (a.timestamp > b.timestamp)
							return 1;
						if (b.timestamp > a.timestamp)
							return -1;
						return 0; 
				})
				.transition()
				.duration(duration)
				.attr("x", function(d, i) {
							return dateScale(d.timestamp)+15;
				})
				.attr("width", function(d) {
							return dateWidthScale(d.number)+1;
				})
				.transition()
				.attr("y", 0);	
		};
				
		//Method called when the separated radio button is selected
		separatedView = function() {
			
			var currentOpenLength, 
				currentActiveLength, 
				currentIdleLength, 
				currentBustedLength;
			
			//Ensures the correct axis for the view is in use
			xAxisGroup.call(hoursAxis);
			
			//Offsets all the current lengths by the left padding.
			currentOpenLength = padding/2; 
			currentActiveLength = padding/2;  
			currentIdleLength = padding/2;  
			currentBustedLength = padding/2; 
			
			//Makes the container larger as this view is stacked (one bar ontop of another on top of another)			
			svgContainer.attr("height", eventTypes.length*barwidth+100);
			
			//Moves the legend to the correct position
			legend.transition()
				.duration(duration)
				.attr("transform", "translate(" + padding/2 + "," + (barwidth*eventTypes.length + 25) + ")");
			
			//Moves the axis to the correct position
			xAxisGroup.transition()
					.duration(duration)
					.attr("transform", "translate(" + padding/2 + "," + barwidth*eventTypes.length + ")");
			
			//Sorts the bar sections by eventType and then displays them vertically in groups	
			svgContainer.selectAll("#bar" + id + " " + ".barsection")
					.sort(function (a, b) {
						var acomp, bcomp;
						acomp = getValueForType(a.eventType);
						bcomp = getValueForType(b.eventType);
						if (acomp > bcomp)
							return 1;
						if (bcomp > acomp)
							return -1;
						return 0; 
					})
					.transition()
					.duration(duration)
					.attr("y", function(d) {
						if (d.eventType == "Computer Active") {
							return 0;
						}
						else if (d.eventType == "Computer Idle") {
							return barwidth*1;
						}
						else if (d.eventType == "Computer Busted") {
							return barwidth*2;
						}
					})
					.transition()
					.attr("width", function(d) {
								return scale(d.number)+1;
						})
					.attr("x", function(d) {
								if (d.eventType == "Computer Open") {
									var returnOpen = currentOpenLength;
									currentOpenLength = currentOpenLength + scale(d.number);
									return returnOpen;
								}
								else if (d.eventType == "Computer Active") {
									var returnActive = currentActiveLength;
									currentActiveLength = currentActiveLength + scale(d.number);
									return returnActive;
								}
								else if (d.eventType == "Computer Idle") {
									var returnIdle = currentIdleLength;
									currentIdleLength = currentIdleLength + scale(d.number);
									return returnIdle;
								}
								else if (d.eventType == "Computer Busted") {
									var returnBusted = currentBustedLength;
									currentBustedLength = currentBustedLength + scale(d.number);
									return returnBusted;
								}
							});
		};
				
		//Method called when the grouped radio button is selected
		groupedView = function() {
			
			//Offsets the current position by the left padding
			var currentGroupPosition = padding/2;
			
			//Ensures the correct axis is in use
			xAxisGroup.call(hoursAxis);
			
			//Ensures the legend is in the correct position		
			legend.transition().delay(duration)
						.duration(duration)
						.attr("transform", "translate(" + padding/2 + "," + (barwidth + 25) + ")");
			
			//Ensures the axis is in the correct position	
			xAxisGroup.transition().delay(duration)
					.duration(duration).attr("transform", "translate(" + padding/2 + "," + barwidth + ")");
					
			//Sorts the bar sections by event type and then positions them horizontally in groups.
			svgContainer.selectAll("#bar" + id + " " + ".barsection")
					.sort(function (a, b) {
						var aValue = getValueForType(a.eventType);
						var bValue = getValueForType(b.eventType);
						if (aValue > bValue)
							return 1;
						if (bValue > aValue)
							return -1;
						return 0; 
					})
					.transition()
					.duration(duration)
					.attr("width", function(d) {
								return scale(d.number)+1;
						})
					.attr("x", function(d) {
								var returnT = currentGroupPosition;
								currentGroupPosition = currentGroupPosition + scale(d.number);
								return returnT;	
								})
					.transition()
					.attr("y", 0);
		}; 
				
		//Function to display the tooltip
		displayToolTip = function(d) {
			
			//function to calculate the left coordinate of the tooltip	
			var left = function() {
							if(d3.event.pageX + $(".tooltip").width() > window.innerWidth) {
								return window.innerWidth - $(".tooltip").width();
							} 
							else {								
								return d3.event.pageX;
								}
							};
			
			//Variable for the top coordinate of the tooltip						
			var top = $("#bar"+id).position().top-$(".tooltip").height()-10 + "px";
			
			//Turn the tooltip opaque (so its not invisible)	
			tooltip.transition()
					.duration(200)
					.style("opacity", 1);
			
			//Calculate the html to be displayed in the tooltip			
			tooltip.html(function() {
					var returnString = "<p>Event: " + d.eventType + "</p><p>Time: ~ " + 
										Math.floor((d.number)/60) + " minutes</p>" +
										"<p> Date: " + d.timestamp.toString().substring(0, 24) + "</p>";
										if (d.eventType == "Computer Busted") {
											returnString = returnString + "<p> Busted By: " + d.bustedBy + "</p>";
										}
					return returnString;
					}); 
			
			//Position the tooltip using css	
			$(".tooltip").css({top: top, left: left});
			
		};
	
		//Helper function for sorting by eventType
		getValueForType = function(eventType) {
			switch (eventType) {
				case "Computer Open":
					return 1;
				case "Computer Active":
					return 2;
				case "Computer Idle":
					return 3;
				case "Computer Busted":
					return 4;
			}
		};	
	
		/* 
		 * FUNCTION CALLS
		 *  
		 * Any calls to the functions defined or to miscellanous D3 functions are in this section
		 * 
		 * */		
						
		//Adds event listeners to each of the radio buttons on the stats page.
		d3.selectAll("#bar" + id + " #separated").on("change", separatedView);
		d3.selectAll("#bar" + id + " #grouped").on("change", groupedView);
		d3.selectAll("#bar" + id + " #chronological").on("change", chronologicalView);
				
		//The default view for the bar visualisation	
		chronologicalView();     
							
	};
	
	return {
		draw: draw
			};
}());
