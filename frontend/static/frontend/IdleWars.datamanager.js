var IdleWars;
if (!IdleWars) {
    IdleWars = {};
}

/*
 * 
 * Datamanager loads the json describing sessions for a user with the id input_id 
 * The json is then formatted to be used by the barvisualisation before the barvisualisation is called with the data.
 * Datamanger also sets up the datepickers and filter button for the bar visualisation.
 * 
 */
IdleWars.datamanager = (function() {
	'use strict';
	
	var get_events;
		
	get_events = function(input_id) {
		var number_of_events,
			events_array,
			returnArray,
			total_time;
			
		function getJsonData() {
			return $.ajax({
				url: g_server_url + "api/userstats/events/" + input_id
			});
		}
		
		$.when(getJsonData()).done(function(a1) {
			var first_date, last_date, final_date, dateformat, i, eventType, timestamp;
			if (a1 !== null) {
				events_array=a1;
				dateformat = d3.time.format.utc("%Y-%m-%d %H:%M:%S");
				total_time = 0;
				returnArray = [];
				
				//for (i = 0; i < events_array.length - 1;  i+= 1) {
				for (i = 0; i < events_array.length;  i+= 1) {
					timestamp = events_array[i].timestamp.substring(0, 19);
					timestamp = dateformat.parse(timestamp);
					total_time = total_time+events_array[i].length;
					switch(events_array[i].eventType)
					{
						case "CA":
								eventType = "Computer Active";
								break;
						case "CO":
								eventType = "Computer Open";
								break;
						case "CI":
							switch(events_array[i].bustedBy)
							{
								case null:
									eventType = "Computer Idle";
									break;
								default:
									eventType = "Computer Busted";
									break;
							}
							break;
					} 
					returnArray.push({ 
						"eventType": eventType, 
						"number":  events_array[i].length, 
						"bustedBy": events_array[i].bustedBy, 
						"timestamp" : timestamp
						});
				}
				returnArray.sort(function (a, b) {
					if (a.timestamp > b.timestamp) {
						return 1;
					}
					if (b.timestamp > a.timestamp) {
						return -1;
					}
					return 0; 
				});
				if (returnArray[0] !== undefined) {
					first_date = returnArray[0].timestamp;
					last_date = returnArray[returnArray.length-1].timestamp;
					final_date = new Date(last_date.getTime() + returnArray[returnArray.length-1].number*1000);
					$(function() {
						$( "#datepickerfrom" + input_id).datepicker({
							dateFormat: "dd-M-yy",
							defaultDate: first_date,
							minDate: first_date,
							maxDate: final_date,
							onSelect: function(date) {
								var date2 = $("#datepickerfrom" + input_id).datepicker('getDate');
								$("#datepickerto" + input_id).datepicker('option', 'minDate', date2);
							}
						});
						$( "#datepickerfrom" + input_id).datepicker("setDate", first_date);
						$( "#datepickerto" + input_id).datepicker({
							dateFormat: "dd-M-yy",
							defaultDate: new Date(final_date.getTime() + (24 * 60 * 60 * 1000)),
							minDate: first_date,
							maxDate: new Date(final_date.getTime() + (24 * 60 * 60 * 1000)),
							onClose: function() {
								var date1 = $("#datepickerfrom" + input_id).datepicker('getDate');
								var date2 = $("#datepickerto" + input_id).datepicker('getDate');
								
								if (date2 < date1) {
									var minDate = $("#datepickerto" + input_id).datepicker('option', 'minDate', date1)
									var newDate = Date.parse(minDate);
									$("#datepickerto" + input_id).datepicker('setDate', newDate);
								}
							}
						});
						$( "#datepickerto" + input_id).datepicker("setDate", new Date(final_date.getTime() + (24 * 60 * 60 * 1000)));
					});
					
					$("#filter" + input_id).click(function() {
						var fromDate = $("#datepickerfrom" + input_id).datepicker('getDate');
						var toDate = $("#datepickerto" + input_id).datepicker('getDate');
						if ((fromDate && toDate) && (fromDate.getTime() != toDate.getTime())) {
							var newArray = returnArray.filter(function(d) { 
								var timestampEnd = new Date(d.timestamp.getTime()+(d.number*1000));
								if ((d.timestamp >= fromDate && d.timestamp <= toDate) || timestampEnd >= fromDate && timestampEnd <= toDate) {
									return true;
								}
																		});
							
							/*
							 * 
							 * Bugs to look at:
							 * The first tick mark gets cropped so can't be read
							 */
							$("#bar" + input_id + " svg").remove();
							IdleWars.barvisualisation.draw(newArray, (toDate.getTime()-fromDate.getTime())/1000, input_id, fromDate, toDate);
						}
					});
						
					IdleWars.barvisualisation.draw(returnArray, total_time, input_id, first_date, final_date);
				}
			
			} else {
				number_of_events="Error";
				events_array="Error";
			}
		});
					
		return {
			total_number_of_events: function() { return number_of_events;},
			events_array: function() { return events_array;}
		};
	};

	return {
		draw_bar_vis: get_events
	};
}());
