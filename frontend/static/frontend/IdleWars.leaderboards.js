var IdleWars;
if (!IdleWars) {
    IdleWars = {};
}

/*
 * Leaderboards loads json produced by the server and then displays it in 
 * three columns: Lowest Idle time, busted count and busted time.
 * 
 */

IdleWars.leaderboards = (function () {
    var load_leaderboards,
        display_leaderboards;

	//Load Leaderboards is a function that loads the json for the leaderboards frokm the api	
	load_leaderboards = function () {
		return $.ajax({
			url: g_server_url + "api/userstats/leaderboards"
			});
	};
	
	/* display_leaderboards takes a boolean parameter limited. If limited is true, the leaderboard 
	 * length is limited by the number of entries it can display on the screen. 
	 */
	display_leaderboards = function (limited,page_username){  
		
		var window_height,
			top_offset,
			number_of_spaces;
		

		//calculates the height of the window and the space the headers take up before 	
		window_height = $(document).height();
		top_offset = $(".container").height() + $(".row").height();
		
		//64 is the height of the image and 15 the height of the top margin for the container.
		//really this should be calculated using jQuery rather than hardcoded but I can't get it to work
		number_of_spaces = Math.floor((window_height-top_offset)/(64+15));
		
		
			load_leaderboards().done(function (user_data_array, status,request_response){
				user_data_array.busted_count = jQuery.grep(user_data_array.busted_count, function(value) {
					  return value.username !== "mario" && value.username !== "sonic";
					});
				user_data_array.busted_time = jQuery.grep(user_data_array.busted_time, function(value) {
					  return value.username !== "mario" && value.username !== "sonic";
					});
				user_data_array.lowest_idle_time = jQuery.grep(user_data_array.lowest_idle_time, function(value) {
					  return value.username !== "mario" && value.username !== "sonic";
					});
			
			var 
				lowest_idle_time,
				busted_others,
				busted_time,
				idle_time_spaces,
				busted_time_spaces,
				busted_others_spaces,
				
				//functions
				calculate_spaces,
				html_generator;
			
			//If the number of entries in the array is less than the length of the array return the length of the array
			//This function prevents errors such as trying to access index 7 in an array of length 5.	
			calculate_spaces = function(array){
				if (array.length < number_of_spaces) {
					return array.length;
				}
				else {
					return number_of_spaces - 1;
				}
			};
			
			lowest_idle_time = user_data_array.lowest_idle_time;
			busted_others = user_data_array.busted_count;
			busted_time = user_data_array.busted_time;
			
			//filters out any entries with 0 as 0 isnt really suited for these two leaderboards
			busted_others = busted_others.filter(function (d) {return d.noOfIdleCompBusted > 0;});
			busted_time = busted_time.filter(function (d) {return d.idleTime > 0;});
			
			//If the leaderboard is limited, calculate how many spaces each leaderboard has.
			if (limited) {
				idle_time_spaces = calculate_spaces(lowest_idle_time);
				busted_others_spaces = calculate_spaces(busted_others);
				busted_time_spaces = calculate_spaces(busted_time);
			} else {
				/*busted_others_spaces = busted_others.length - 1;
				busted_time_spaces = busted_time.length - 1;
				idle_time_spaces = lowest_idle_time.length - 1;*/
				
				busted_others_spaces = busted_others.length;
				busted_time_spaces = busted_time.length;
				idle_time_spaces = lowest_idle_time.length;
				
			}
			
			//Html_generator is a function that takes an array, a number of spaces and a name and creates and then outputs
			//the html for that respective leaderboard.
			html_generator = function(leaderboard_array, leaderboard_spaces, leaderboard_name) {
					
				var html, i;
					
				//Hide the loader for the leaderboard
				$('#' + leaderboard_name + 'Loader').hide();
					
				//No entries in the leaderboard so display a message
				if(leaderboard_spaces <= 0) {
					html = '';
					html += '<div class="media">';
					html += '<div class="media-body">';
					html += '<h4 class="media-heading"> No Entries </h4>';
					html += '</div>';
					html += '</div>';
						$('#' + leaderboard_name + 'holder').append(html);
				}
				else {
					
					for (i= 0; i< leaderboard_spaces; i++){
						html = '';
						if (leaderboard_array[i].username === page_username) {
							html += '<div class="media selected">';
						} 
						else {
							html += '<div class="media">';
						}
						html += '<a class="pull-left" href="#" style="width: 67px;">';
						html += '<img class="media-object" src="'+g_server_url+'uploads/sml_' + leaderboard_array[i].imgurl + '">';
						html += '</a>';
						html += '<div class="media-body">';
						html += '<h4 class="media-heading">'+leaderboard_array[i].username+'</h4>';
						
						//Depending on the name display a different message
						switch(leaderboard_name) {
							case "noOfComputersBusted":
								html += leaderboard_array[i].noOfIdleCompBusted+' times';
								break;
							case "minutesOfIdleTimeBusted":
								html += 'Idle Time: ' + busted_time[i].idleTime + ' minutes';
								break;
							case "lowestIdleTime":
								//html += 'Percent of Idle Time: ' + lowest_idle_time[i].idleTime.toFixed();
								//html += ' of ' + lowest_idle_time[i].totalTime.toFixed();
                                if (lowest_idle_time[i].totalTime != 0){// Protect from division with zero
                                    html += 'Idle Time: ' +((lowest_idle_time[i].idleTime *100)/lowest_idle_time[i].totalTime).toFixed() + "%";
                                }else{
                                    html += 'Idle Time: ' +'0%';

                                }

								break;
						}
						html += '</div>';
						html += '</div>';
						$('#' + leaderboard_name + 'holder').append(html);
					}
					
					//Set each image to 64px by 64px to prevent alignment issues.
					$('#' + leaderboard_name + 'holder img').each(function () {
						var self = $(this);
						if (self.width() > self.height()) {
							self.css('width', '64px');
						} 
						else {
							self.css('height', '64px');
						}
					});  
				}
			};
			
			//Generate html for the three different leaderboards
			html_generator(busted_others, busted_others_spaces, "noOfComputersBusted");
			html_generator(busted_time, busted_time_spaces, "minutesOfIdleTimeBusted");
			html_generator(lowest_idle_time, idle_time_spaces, "lowestIdleTime");
		
		});
   
	};
			
	return {
		load: display_leaderboards
	};
	
}());