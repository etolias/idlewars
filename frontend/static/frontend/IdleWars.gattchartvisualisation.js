example("2014-02-01","2014-02-15");

function example(start_date,end_date) {
    var tasks = [];
    var taskNames=[];
    //d3.json("../sessions.json", function(error,data){
    d3.json(g_server_url+"api/userstats/sessionsfiltered/"+start_date+"/"+end_date, function(error,data){
        console.log(data);
         data.map(function(d){
             if ((d.username === "mario") || (d.username === "sonic"))return;
             /*console.log("====================================");
             console.log(d.start_event);
             console.log(d.end_event);
             console.log("====================================");
             */
             if (d.sessionType === "CA"){
                tasks.push({
                    "startDate":new Date(d.start_event),
                    "endDate": new Date(d.end_event),
                    "taskName": d.username,
                    "status": "RUNNING"

                           });
            }
            else if (d.sessionType === "CI") {
                if (d.is_busted_by === "False")
                 tasks.push({
                    "startDate": new Date(d.start_event),
                    "endDate": new Date(d.end_event),
                    "taskName": d.username,
                    "status": "KILLED"
                });
                else if(d.is_busted_by === "True"){
                    tasks.push({
                    "startDate": new Date(d.start_event),
                    "endDate": new Date(d.end_event),
                    "taskName": d.username,
                    "status": "FAILED"
                });

                }


            }

            if ($.inArray(d.username,taskNames)===-1){
                taskNames.push(d.username);
            }


        });
        console.log(data);
        
        var taskStatus = {
        "SUCCEEDED" : "bar",
        "FAILED" : "bar-failed",
        "RUNNING" : "bar-running",
        "KILLED" : "bar-killed"
    };
    
    //var taskNames = [  "pszkb", "pszab", "suckerforresearch", "rjbyrne","sonic","ps001bk","rjbyrne","growlingfish","Bronya","Toad","Angeles","angelesmc","mario", "Lex Lota" ];
    taskNames.sort();
    tasks.sort(function(a, b) {
        return a.endDate - b.endDate;
    });
    var maxDate = tasks[tasks.length - 1].endDate;
    tasks.sort(function(a, b) {
        return a.startDate - b.startDate;
    });
    var minDate = tasks[0].startDate;
    
    //var format = "%H:%M";
    var format = "%a %b %e %X %Y";
        
    var gantt = d3.gantt().taskTypes(taskNames).taskStatus(taskStatus).tickFormat(format);
    gantt(tasks);
    //d3.selectAll(".x axis").selectAll("text").attr("transform", "rotate(90)");    
    });
    
};

