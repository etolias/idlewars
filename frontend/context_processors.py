from frontend.models import UserProfile
from basicutils.djutils import get_requested_user
from django.conf import settings
"""
Default context processor, returns a context containing owner, which is the user logged in and 
all the user profiles in the system that own a computer (this used to be used to display the side bar)
"""
def default_context(request):
    owner = get_requested_user(request)
    allUserProfiles = UserProfile.objects.filter(frontend_computer_owners__isnull=False)
    context = {'owner':owner, 'allUserProfiles':allUserProfiles, 'DEPLOYMENT_NAME':settings.DEPLOYMENT_NAME}
    return context