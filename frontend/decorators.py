'''
Created on 22 Dec 2011

@author: enrico
'''
import urlparse
from django.conf import settings
from django.http import HttpResponseForbidden
from django.utils.decorators import available_attrs
from django.contrib.auth.decorators import REDIRECT_FIELD_NAME
from telnetlib import AUTHENTICATION
from basicutils.djutils import get_json_authentication_failure
try:
    from functools import wraps
except ImportError:
    from django.utils.functional import wraps  # Python 2.4 fallback.

from basicutils.djutils import get_json_error, can_access_user_data

def access_required(view_func):
    login_url=None
    redirect_field_name=REDIRECT_FIELD_NAME
    @wraps(view_func, assigned=available_attrs(view_func))
    def _wrapped_view(request, *args, **kwargs):
        if request.user.is_authenticated():
            # does the request contain a user?
            owner = request.REQUEST.get('user')
            # can the user access the data?
            if can_access_user_data(owner, request.user):
                return view_func(request, *args, **kwargs)
            else:
                return HttpResponseForbidden(get_json_error('ACCESS_DENIED'))
        path = request.build_absolute_uri()
        # If the login url is the same scheme and net location then just
        # use the path as the "next" url.
        login_scheme, login_netloc = urlparse.urlparse(login_url or
                                                    settings.LOGIN_URL)[:2]
        current_scheme, current_netloc = urlparse.urlparse(path)[:2]
        if ((not login_scheme or login_scheme == current_scheme) and
            (not login_netloc or login_netloc == current_netloc)):
            path = request.get_full_path()
        from django.contrib.auth.views import redirect_to_login
        return redirect_to_login(path, login_url, redirect_field_name)
    return _wrapped_view

def admin_access_required(view_func):
    login_url=None
    redirect_field_name=REDIRECT_FIELD_NAME
    @wraps(view_func, assigned=available_attrs(view_func))
    def _wrapped_view(request, *args, **kwargs):
        if request.user.is_authenticated():
            # does the request contain a user?
            owner = request.REQUEST.get('user')
            # can the user access the data?
            if can_access_user_data(owner, request.user) and request.user.is_superuser:
                return view_func(request, *args, **kwargs)
            else:
                return HttpResponseForbidden(get_json_error('ACCESS_DENIED'))
        path = request.build_absolute_uri()
        # If the login url is the same scheme and net location then just
        # use the path as the "next" url.
        login_scheme, login_netloc = urlparse.urlparse(login_url or
                                                    settings.LOGIN_URL)[:2]
        current_scheme, current_netloc = urlparse.urlparse(path)[:2]
        if ((not login_scheme or login_scheme == current_scheme) and
            (not login_netloc or login_netloc == current_netloc)):
            path = request.get_full_path()
        from django.contrib.auth.views import redirect_to_login
        return redirect_to_login(path, login_url, redirect_field_name)
    return _wrapped_view

def http_basic_auth(func):
    @wraps(func)
    def _decorator(request, *args, **kwargs):
        from django.contrib.auth import authenticate, login
        if request.META.has_key('HTTP_AUTHORIZATION'):
            authmeth, auth = request.META['HTTP_AUTHORIZATION'].split(' ',1)
            if authmeth.lower() == 'basic':
                auth = auth.strip().decode('base64')
                username, password = auth.split(':',1)
                user = authenticate(username=username, password =password)
                if user:
                    login (request,user)
                else:
                    from django.http import HttpResponse
                    r = HttpResponse (get_json_authentication_failure(), status = 400)
                    r["Content-Type"]="application/json";
                    r["WWW-Authenticate"]="Basic";
                    r["realm"]="UserLogin"
                    return r;
                    """"r = HttpResponse ('Auth Required', status = 401)
                    r["WWW-Authenticate"]="Basic"
                    r["realm"]="UserLogin"
                    return r"""
        else:
            from django.http import HttpResponse
            r = HttpResponse ('Auth Required', status = 401)
            r["WWW-Authenticate"]="Basic"
            r["realm"]="UserLogin"
            return r
        return func(request,* args, ** kwargs)
    return _decorator         

def basic_http_auth(f):
    @wraps(f)
    def _decorator (request,*args, **kwargs):
        if request.META.get('HTTP_AUTHORIZATION',False):
            authtype,auth = request.META['HTTP_AUTHORIZATION'].split(' ')
            import base64
            auth = base64.b64decode(auth)
            username, password = auth.split(':')
            if username == 'foo' and password == 'bar':
                return f(request, * args, **kwargs)
            else:
                from django.http import HttpResponse
                r = HttpResponse ('Auth Required', status = 401)
                r['WWW-Authenticate']='Basic realm="bat"'
                return r
            r = HttpResponse("Auth Required", status = 401)
            r["WWW-Authenticate"] = 'Basic realm="bat"'
            return r
        return _decorator
            
            