'''
Created on 20 May 2013

@author: Vangelis
'''
from django import forms
from frontend.models import Computer, UserProfile
from django.forms.util import ErrorList
from registration.forms import RegistrationFormTermsOfService
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

def validate_computer_not_none(value):
    if value == '0':
        raise ValidationError('please choose computer type')

class ComputerRegistrationForm(forms.Form):
    name = forms.CharField(label='name',min_length=3)
    url = forms.URLField(label='url', max_length=30)
    
    """
    TODO: change this so its more lik the clean method below? I think thats better practice.
    """
    def is_valid(self, owner):
        valid = super(ComputerRegistrationForm, self).is_valid()
        userProfileOfOwner = UserProfile.objects.get(user=owner)
        noOfcomputers = Computer.objects.filter(owner=userProfileOfOwner).count()
        if (noOfcomputers!=0):
            errors = self._errors.setdefault("__all__", ErrorList())
            errors.append("You are not allowed to register more than one computer")
            return False
        return valid
            
class UserRegistrationForm(RegistrationFormTermsOfService):
    #Prevent the user from providing the username with spaces and other tricky characters
    username= forms.CharField(label="username", min_length=4, validators=[RegexValidator(r'^[\w]+$', ('Enter a valid username. Valid characters: a-zA-Z0-9_'), 'invalid')], error_messages={'requied': 'Enter a valid username. Valid characters: a-zA-Z0-9_'})
    #username= forms.CharField(label="username", min_length=4)
    password1 = forms.CharField(label='password', widget=forms.PasswordInput())
    password2= forms.CharField(label='password (again)', widget= forms.PasswordInput())
    
    first_name = forms.CharField(label="first_name")
    last_name = forms.CharField(label="last_name")
    email = forms.EmailField(label="email")
    user_image = forms.ImageField(label="user_image")
    computer_type_choices = (
        ('0', 'none'),
        ('1', 'Desktop'),
        ('2', 'Laptop'),
    )
    computer_type = forms.ChoiceField(choices=computer_type_choices, label="computer_type", validators=[validate_computer_not_none])




    def clean(self):
        cleaned_data = super(UserRegistrationForm, self).clean()
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        computer_type = self.cleaned_data.get("computer_type")


        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")

        return cleaned_data
    
#class IldeRegistrationForm(forms.Form):
#    start = forms.DateTimeField(input_formats=DATE_FMTS)
#    end= forms.DateTimeField(input_formats=DATE_FMTS)
#    bustedBy = 
#    forComputer