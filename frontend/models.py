from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.utils.timezone import utc
import random
import string
from qrcode import QRCode, ERROR_CORRECT_H
from cStringIO import StringIO
from django.core.files import File
from django.core.files.base import ContentFile

from os import rename
from django.conf import settings

# Create your models here.

#adding Admin interface.
class Link(models.Model):
    url = models.URLField(unique=True)
    def __str__(self):
        return self.url
    class Admin:
        pass 


#With this class I am trying to extend in a safe way the User model and add an image for each user.
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    #Other fielsd are
    image= models.ImageField(upload_to="profileImg/", blank=True,null=True)
    qrcode=models.ImageField(upload_to="qrcodes/",blank=True,null=True)
    
class Computer (models.Model):
    EVENT_TYPES=(
                ("CA","Computer Active"),#Defines that the user moved the mouse or pressed the key.
                ("CI","Computer Idle"),#this happens when there is no user activity
                
    )
    COMPUTER_TYPES = (
        ("1","Desktop"),
        ("2","Laptop"),
    )
    #name of the computer
    name=models.CharField( max_length=30, unique=True, db_index=True)
    #URL of the computer
    url=models.CharField(max_length=100)
    #The owner of the computer
    owner=models.ForeignKey(UserProfile, related_name='frontend_computer_owners')
    #TODO: Current Busted by (this will allow better performance of sessions lookup)
    currentBustedBy= models.ForeignKey(UserProfile,null=True,blank=True,related_name='frontend_computer_currentBustedBy')
    #type of the computer (two options desktop or laptop)
    typeOfComputer = models.CharField(max_length=1, choices=COMPUTER_TYPES, blank=False)
    #represents the current status of the computer CA, CI
    currentEventType = models.CharField(max_length=2, choices=EVENT_TYPES, default="CA")

class Event (models.Model):
    """
        related_name='IdlePeriod_owner'
        This class has two foreign keys to User. Django automatically creates a reverse relation from User back to GameClaim, 
        which is usually IdlePeriod_set. However, because you have two FKs, you would have two IdlePeriod_set attributes which is obviously impossible
        So you need to tell Django what name to use for the reverse relation.
        http://stackoverflow.com/questions/1142378/django-why-some-fields-clashes-with-other
    """
    EVENT_TYPES=(
                 ("CO","Computer Open"),#This happens when the computer was off and turnd on
                ("KP","Key Pressed"),
                ("MM","Mouse Moved"),
                ("MS","Mouse Scroll"),
                ("CA","Computer Active"),#Defines that the user moved the mouse or pressed the key.
                ("CI","Computer Idle"),#this happens when there is no user activity
                #("CS","Computer "),
                ("CH","Computer Hibernate"),#The computer swiched into hibernate mode
                ("HW","Hibernate Wake up"),#The computer woke up from hibernate mode
                ("SW","Stand by Wake up"),
    )
    macAddress=models.CharField(max_length=18)
    ipAddress=models.CharField(max_length=16)
    appver= models.CharField(max_length=10)
    #The windows version needs 11 chars so for safety i added ather 10
    os=models.CharField(max_length=20)
    clientTimestamp = models.DateTimeField(null=True, blank=True, db_index=True)
    timestamp=models.DateTimeField(db_index=True)
    
    #A string that represents the type of the event.
    eventType=models.CharField(max_length=2,choices=EVENT_TYPES,default="CA")
    
    #The computer the event belongs to
    forComputer= models.ForeignKey(Computer)
    
    #In case of "Computer Idle" is set to the user that busted the computer otherwise it must be null
    #A better option is to use another object but for sake of simplicity we use this. 
    bustedBy = models.ForeignKey(UserProfile,null=True,blank=True)
    
    class Meta:
        unique_together = (('timestamp', 'forComputer'), )
        ordering = ['-timestamp']
    
    def save(self, *args, **kwargs):
        if self.timestamp == None:
            self.timestamp = datetime.utcnow().replace(tzinfo=utc)
        super(Event, self).save(*args, **kwargs)
    
#class IdleEvent (Event):
#    #The user that busted this idle period
#    bustedBy = models.ForeignKey(User,null=True,blank=True)

class Session(models.Model):
    start_event = models.ForeignKey(Event, related_name="start_event")
    end_event = models.ForeignKey(Event, related_name="end_event")
    length = models.IntegerField()
    start_timestamp = models.DateTimeField()
    end_timestamp = models.DateTimeField()
    
    def save(self, *args, **kwargs):
        self.start_timestamp = self.start_event.timestamp
        self.end_timestamp = self.end_event.timestamp
        super(Session, self).save(*args, **kwargs)
        

class BustManager(models.Manager):
    def update_profile(self,userprofile):
        bust_hash = hash_generator()
        while BustProfile.objects.filter(bust_hash=bust_hash).count() > 0:
            bust_hash = hash_generator()
        qr = QRCode(version=4, error_correction=ERROR_CORRECT_H)
        qr.add_data(settings.ROOT_URL+"bust/" + bust_hash)
        #qr.add_data("http://game.ecs.soton.ac.uk"+"/IdleWars/bust/" + bust_hash)
        qr.make() 
        im = qr.make_image()  
        image_buffer = StringIO()
        im.save(image_buffer)
        image_buffer.seek(0)      
        file_name = userprofile.user.username+".png"
        #file_name = file_name.replace(" ", "\ ")
        file_object = File(image_buffer, file_name)
        content_file = ContentFile(file_object.read())
        userprofile.qrcode.delete()
        userprofile.qrcode.save(file_name, content_file, save=True)
        userprofile.save()
        
        rename(userprofile.qrcode.file.name, userprofile.qrcode.file.name.replace('_',' '))
        #userprofile.qrcode.name = 'qrcodes/%s' % file_name
        #userprofile.save()
        
        bustProfile, created = BustProfile.objects.get_or_create(userprofile=userprofile)
        bustProfile.bust_hash = bust_hash
        bustProfile.save()

class BustProfile(models.Model):
    userprofile = models.OneToOneField(UserProfile)
    bust_hash = models.CharField(max_length=6, unique=True, db_index=True)
    objects = BustManager()
    
def hash_generator(size=6, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for x in range(size))