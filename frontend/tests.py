"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.


Some useful instructions on testing https://docs.djangoproject.com/en/1.2/topics/testing/

"""

from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from django.utils import simplejson
from datetime import datetime

from frontend.models import Computer, Event, UserProfile, Session
from API.views import update_sessions_from_events2, update_sessions
from django.utils.timezone import utc

class SimpleTest(TestCase):
    fixtures = ['data', 'ec_events']
    
    def setUp(self):
        TestCase.setUp(self)
        #Create a test user for testing
        
    def test_segmentation(self):
        u = User.objects.get(username='enrico')
        
        events = Event.objects.filter(forComputer__owner__user=u)
        print events.count()
        
        from API.views import update_sessions
        update_sessions(Computer.objects.get(owner__user=u))

    def test_noise(self):
        u = User.objects.get(username='enrico')
        
        events = Event.objects.filter(forComputer__owner__user=u)
        #start = datetime(2013,8,14,15,0)
        #events = events.filter(timestamp__gte=start)
        print events.count()
        
        events_data = [x for x in events.values_list('timestamp', 'eventType', 'bustedBy')]
        from matplotlib import pyplot as plt
        fig = plt.figure()
        lut = {'CA': 1, 'CI': 0}
        plt.plot([x[0] for x in events_data], [lut[x[1]] for x in events_data], '.-')
        fig.autofmt_xdate()
        plt.show()

    def test_update(self):
        u = User.objects.get(username='enrico')
        
        events = Event.objects.filter(forComputer__owner__user=u)
        dt = datetime(2013,8,14,9,45)
        
        events = events.filter(timestamp__lt=dt)
        
        from API.views import update_sessions_from_events
        update_sessions_from_events(Computer.objects.get(owner__user=u), events)
        
        sessions = Session.objects.filter(start_event__forComputer__owner__user=u)
        sessions = sessions.order_by('start_timestamp')
        
        for s in sessions:
            print s.start_timestamp, s.end_timestamp
        
        
#        events_data = [x for x in events.values_list('timestamp', 'eventType', 'bustedBy')]
#        from matplotlib import pyplot as plt
#        fig = plt.figure()
#        lut = {'CA': 1, 'CI': 0}
#        plt.plot([x[0] for x in events_data], [lut[x[1]] for x in events_data], '.-')
#        fig.autofmt_xdate()
#        plt.show()

        
class ObjectCreation(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        #Create a test user for testing
        username='test'
        password='test'
        my_admin = User.objects.create_superuser(username, 'test@test.com', password)
        
        self.username=username
        self.password=password
        self.user = User.objects.get(username=self.username)
    def test_creation_of_user(self):
        """
            python manage.py test frontend.ObjectCreation.test_creation_of_user
        """
        print "\nCreate User TEST\n"
        img_file = open('test.jpg', 'rb')
        response = self.client.post("/accounts/register/", {
                                          'username': "untest",
                                          'password1': "password", 
                                          'password2': "password", 
                                          'first_name': "fntest", 
                                          'last_name': "lntest", 
                                          'email': "email@example.com",
                                          'user_image': img_file
                                          })
        open('resp.html', 'w').write(str(response))
        self.assertEqual(response.status_code,201)
        usr = User.objects.get(username="untest")
        self.assertIsNotNone(usr)
            
    def test_creation_of_computer(self):
        """    
        
            Test the creation of a computer object
            
            For testing only this 'test method' type:
            python manage.py test frontend.ObjectCreation.test_creation_of_computer
            
        """
        
        print "\nCreate Computer TEST\n"
        self.client.login(username=self.username, password=self.password)
        name="testComp"
        url="http://www.test.com"
        response = self.client.post("/computers/", {
                                          'name': name,
                                          'url': url 
                                          })
        #Test the POST functionality
        self.assertEqual(response["Content-Location"], "/computer/testComp/")
        self.assertEqual(response.status_code,201)
        
        #Test Django functionality
        comp=Computer.objects.get(name=name)
        self.assertIsNotNone(comp)
        self.assertEquals(comp.url, url)
        
        #TODO: test with wrong parameters
        
    def test_creation_of_events (self):
        """2012-12-01T00:00:00.000Z
            Test the creation of a IdleEvent object
            python manage.py test frontend.ObjectCreation.test_creation_of_events
        """
        
        print "\nCreate Idle event TEST\n"
        
        self.client.login(username=self.username, password=self.password)
        
        #First crate a computer
        name="testComp"
        url="http://www.test.com"
        #Then create a user profile of the user
        userProfileOfOwner = UserProfile (user=self.user)
        userProfileOfOwner.save()
        comp = Computer(name=name,url=url, owner=userProfileOfOwner)
        comp.save()
        
        #Then test if a testing event can be added into the database.
        response = self.client.post("/rawinput/"+name+"/events/",{
                                                                  'eventType': "CO" 
                                                                  })
        
        #Create a datetime object with timezone support is enabled
        #For more information about this https://docs.djangoproject.com/en/dev/topics/i18n/timezones/#get-current-timezone
        datetimeNow=datetime.utcnow().replace(tzinfo=utc)
        
        #Test the POST functionality
        self.assertEqual(response["Content-Location"], "/computer/testComp/1/")
        self.assertEqual(response.status_code,201)
        
        #Test Django functionality
        parsed = simplejson.loads(str(response.content))
        idlp=Event.objects.get(id=parsed["id"])
        self.assertIsNotNone(idlp)
        
        print "saved hour: %s \n" % (idlp.timestamp)
        print "now hour: %s \n" % (datetimeNow)
        
        print "saved hour: %s \n" % (idlp.timestamp.hour)
        print "now hour: %s \n" % (datetimeNow.hour)
        
        
        self.assertEquals(idlp.timestamp.year, datetimeNow.year)
        self.assertEquals(idlp.timestamp.month, datetimeNow.month)
        self.assertEquals(idlp.timestamp.day, datetimeNow.day)
        self.assertEquals(idlp.timestamp.hour, datetimeNow.hour)
        
        
 #   def test_update_of_idle_events (self):
        """
            Test the creation of a IdleEvent object
            
        """
"""        print "\nCreate Idle event TEST\n"
        self.client.login(username=self.username, password=self.password)
        
        #First crate a computer
        name="testComp"
        url="http://www.test.com"
        comp = Computer(name=name,url=url, owner=self.user)
        comp.save()
        
        #Then create and IdleEvent
        comp=Computer.objects.get(name=comp.name)
        idlp= IdlePeriod(forComputer=comp)
        idlp.save()
        
        #Then test the update event.
        response = self.client.post("/rawinput/%s/%s/" % (comp.name,idlp.id))
        
        #Test the POST functionality
        self.assertEqual(response["Content-Location"], "/computer/testComp/1/")
        self.assertEqual(response.status_code,201)
        
        #Test Django functionality
        parsed = simplejson.loads(str(response.content))
        idlp=IdlePeriod.objects.get(id=parsed["id"])
        self.assertIsNotNone(idlp)
"""

class ObjectUpdate(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        #Create a test user for testing
        username='test'
        password='test'
        my_admin = User.objects.create_superuser(username, 'test@test.com', password)
        
        self.username=username
        self.password=password
        self.user = User.objects.get(username=self.username)
        
        self.clientUsername="test2"
        self.clientPassword="test2"
        
        User.objects.create_superuser(self.clientUsername,'test2.test.com',self.clientPassword)
        self.clientUser= User.objects.get(username=self.clientUsername)
        
    def test_busting_computer (self):
        """2012-12-01T00:00:00.000Z
            Test computer bust functionality
            python manage.py test frontend.ObjectUpdate.test_busting_computer
        """
        
        print "\nBust a computer TEST\n"
        
        self.client.login(username=self.username, password=self.password)
        up = UserProfile(user=self.user)
        up.save()
        #First crate a computer that belongs to user test
        name="testComp"
        url="http://www.test.com"
        comp = Computer(name=name,url=url, owner=up)
        comp.save()
        
        #Add some event data to the computer
        idlp= Event(forComputer=comp,eventType="CA")
        idlp.save()
        
        idlp= Event(forComputer=comp,eventType="CA")
        idlp.save()
        
        #Then create an computer Idle event
        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()
        
        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()
        
        #Login as user test2
        self.client.login(username=self.clientUsername, password=self.clientPassword)
        up = UserProfile(user=self.clientUser)
        up.save()
        response = self.client.post("/bust/"+name+"")
        
        
        print response["Content-Location"]
        
        #Test the POST functionality
        self.assertEqual(response["Content-Location"], "/event/4/")
        self.assertEqual(response.status_code,201)
        
        #Test busting functionality
        idlp.bustedBy
        self.assertIsNotNone(idlp)
        
        #Test Django functionality
        parsed = simplejson.loads(str(response.content))
        idlp=Event.objects.get(id=parsed["id"])
        self.assertIsNotNone(idlp)
        
    def test_busting_computer_using_get (self):
        """2012-12-01T00:00:00.000Z
            Test computer bust functionality
            python manage.py test frontend.ObjectUpdate.test_busting_computer
        """
        
        print "\nBust a computer TEST\n"
        
        self.client.login(username=self.username, password=self.password)
        up = UserProfile(user=self.user)
        up.save()
        #First crate a computer that belongs to user test
        name="testComp"
        url="http://www.test.com"
        comp = Computer(name=name,url=url, owner=up)
        comp.save()
        
        #Add some event data to the computer
        idlp= Event(forComputer=comp,eventType="CA")
        idlp.save()
        
        idlp= Event(forComputer=comp,eventType="CA")
        idlp.save()
        
        #Then create an computer Idle event
        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()
        
        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()
        
        #Login as user test2
        self.client.login(username=self.clientUsername, password=self.clientPassword)
        up = UserProfile(user=self.clientUser)
        up.save()
        response = self.client.get("/bust/"+name+"")
        
        
        print response["Content-Location"]
        
        #Test the POST functionality
        self.assertEqual(response["Content-Location"], "/event/4/")
        self.assertEqual(response.status_code,201)
        
        #Test busting functionality
        idlp.bustedBy
        self.assertIsNotNone(idlp)
        
        #Test Django functionality
        parsed = simplejson.loads(str(response.content))
        idlp=Event.objects.get(id=parsed["id"])
        self.assertIsNotNone(idlp)
class SessionDbTest(TestCase):
    fixtures = ['auth.json', 'user_profile.json', 'frontend_computer.json','frontend_event.json']
    def test_session_extraction_from_db(self):

        """
        python manage.py test frontend.tests:SessionTest.test_session_extraction

        """
        print "test_session_extraction_from_db"

        comp = Computer.objects.all()
        print "computers count", comp.count()
        for c in comp:
            print c.name, " id= ", c.id

        comp= Computer.objects.get(id=11)
        update_sessions_from_events2(comp)
        print "sessions calculated"
        allSessions = Session.objects.all()
        print "sessions no",allSessions.count()

        for ses in allSessions:
            print ses.id,"start:", ses.start_event.id,"end:",ses.end_event.id

        Session.objects.all().delete()

        update_sessions(comp)
        print "=============python session calculation============="
        allSessions = Session.objects.all()
        print "sessions no",allSessions.count()
        for ses in allSessions:
            print ses.id,"start:", ses.start_event.id,"end:",ses.end_event.id

        print "end"
class SessionTest(TestCase):
    #fixtures = ['auth', 'user_profile', 'frontend_computer','frontend_event']

    """
    python manage.py test frontend.tests:SessionTest
    """
    def setUp(self):
        print "\nSessionTest init\n"
        TestCase.setUp(self)

        #Create a test user for testing
        username='test'
        password='test'
        my_admin = User.objects.create_superuser(username, 'test@test.com', password)

        self.username=username
        self.password=password
        self.user = User.objects.get(username=self.username)

        self.clientUsername="test2"
        self.clientPassword="test2"

        #User.objects.create_superuser(self.clientUsername,'test2.test.com',self.clientPassword)
        #self.clientUser= User.objects.get(username=self.clientUsername)

    def test_session_extraction(self):
        User.objects.create_superuser(self.clientUsername,'test2.test.com',self.clientPassword)
        self.clientUser= User.objects.get(username=self.clientUsername)
        fixtures = ['auth.json', 'user_profile.json', 'frontend_computer.json','frontend_event.json']
        """
        python manage.py test frontend.tests:SessionTest.test_session_extraction

        """

        print "\nSession extraction TEST\n"

        self.client.login(username=self.username, password=self.password)
        up = UserProfile(user=self.user)
        up.save()
        #First crate a computer that belongs to user test
        name="testComp"
        url="http://www.test.com"
        comp = Computer(name=name,url=url, owner=up)
        comp.save()

        #Add some event data to the computer
        idlp= Event(forComputer=comp,eventType="CA")
        idlp.save()

        idlp= Event(forComputer=comp,eventType="CA")
        idlp.save()

        #Then create an computer Idle event
        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()

        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()


        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()

        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()

        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()

        idlp= Event(forComputer=comp,eventType="CI")
        idlp.save()


        idlp= Event(forComputer=comp,eventType="CA")
        idlp.save()

        idlp= Event(forComputer=comp,eventType="CA")
        idlp.save()

        #Login as user test2
        self.client.login(username=self.clientUsername, password=self.clientPassword)
        up = UserProfile(user=self.clientUser)
        up.save()

        update_sessions_from_events2(comp)
        allSessions=Session.objects.all()
        sessionList= list(allSessions)

        self.assertEqual(allSessions.count(),2)
        print "SQL After 1 assert"
        self.assertEqual(allSessions[0].end_event.eventType,"CA")
        print "SQL After the 2 assert"
        self.assertEqual(allSessions[1].end_event.eventType,"CI")
        print "SQL After the 3 assert"

        Session.objects.all().delete()

        update_sessions(comp)

        self.assertEqual(allSessions.count(),2)
        print "PYTHON After 1 assert"
        self.assertEqual(allSessions[0].end_event.eventType,"CA")
        print "PYTHON After 2 assert"
        self.assertEqual(allSessions[1].end_event.eventType,"CI")
        print "PYTHON After 3 assert"



        Session.objects.all().delete()
        Event.objects.all().delete()


        dt=datetime(2014, 1, 1, 0, 0, 0).replace(tzinfo=utc)
        idlp= Event(forComputer=comp,eventType="CA", timestamp=dt)
        idlp.save()

        dt=datetime(2014, 1, 1, 0, 0, 1).replace(tzinfo=utc)
        idlp= Event(forComputer=comp,eventType="CA", timestamp=dt)
        idlp.save()

        dt=datetime(2014, 1, 1, 0, 0, 2).replace(tzinfo=utc)
        idlp= Event(forComputer=comp,eventType="CA", timestamp=dt)
        idlp.save()

        dt=datetime(2014, 1, 1, 0, 40, 0).replace(tzinfo=utc)
        idlp= Event(forComputer=comp,eventType="CA", timestamp=dt)
        idlp.save()

        dt=datetime(2014, 1, 1, 0, 40, 1).replace(tzinfo=utc)
        idlp= Event(forComputer=comp,eventType="CA", timestamp=dt)
        idlp.save()

        dt=datetime(2014, 1, 1, 0, 40, 2).replace(tzinfo=utc)
        idlp= Event(forComputer=comp,eventType="CA",timestamp=dt)
        idlp.save()

        dt=datetime(2014, 1, 1, 0, 40, 3).replace(tzinfo=utc)
        idlp= Event(forComputer=comp,eventType="CA",timestamp=dt)
        idlp.save()

        update_sessions_from_events2(comp)
        allSessions=Session.objects.all()
        sessionList= list(allSessions)

        self.assertEqual(allSessions.count(),1)
        print "SQL After 1 assert"
        self.assertEqual(allSessions[0].end_event.eventType,"CA")
        print "SQL After the 2 assert"


        Session.objects.all().delete()

        update_sessions(comp)

        self.assertEqual(allSessions.count(), 1)
        print "PYTHON After 1 assert"
        self.assertEqual(allSessions[0].end_event.eventType,"CA")
        print "PYTHON After the 2 assert"

    def test_session_extraction_from_db(self):

        """
        python manage.py test frontend.tests:SessionTest.test_session_extraction

        """
        print "test_session_extraction_from_db"

        comp = Computer.objects.all()
        print "computers count", comp.count()
        for c in comp:
            print c.name
        #update_sessions_from_events2(comp)
        #allSessions=Session.objects.all()
        #for ses in allSessions:
        #    print ses


class TimestampTest(TestCase):
    fixtures = ['auth', 'computer', 'profiles', 'adil']
    
    def setUp(self):
        TestCase.setUp(self)

    def test_noise(self):
        u = User.objects.get(username='enrico')
        
        events = Event.objects.filter(forComputer__owner__user=u)
        #start = datetime(2013,8,14,15,0)
        #events = events.filter(timestamp__gte=start)
        print events.count()
        
        events_data = [x for x in events.values_list('timestamp', 'eventType', 'bustedBy')]
        from matplotlib import pyplot as plt
        fig = plt.figure()
        lut = {'CA': 1, 'CI': 0}
        plt.plot([x[0] for x in events_data], [lut[x[1]] for x in events_data], '.-')
        fig.autofmt_xdate()
        plt.show()

        
        
class TimestampTest2(TestCase):
    fixtures = ['auth', 'computer', 'profiles', 'vangelis']


