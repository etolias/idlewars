#encoding:UTF-8
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from frontend.models import UserProfile, Computer, Event
import os
import random
from datetime import datetime, timedelta
from django.db import transaction
from django.utils.timezone import utc
from django.core.files import File
from qrcode import QRCode, ERROR_CORRECT_H
from cStringIO import StringIO
import pytz        
from IdleWars.settings import ROOT_URL
from django.core.files.base import ContentFile

class Command(BaseCommand):
    #args = '<poll_id poll_id ...>'
    help = 'Populates the db for the pricing applications'

    # Need to make a user in django.contrib.auth.User - they need to have 
    # a username, password, first name, last name and email
    # then need to take the created user object and give it to the user profile
    # the user profile takes a profile image and a qrcode image
    
    # Need to create computers and associate them with those users
    # Name the computers and give the computer a url current location/computer/name
    
    # Need to generate events of capturing, idle etc etc, associate the event with a computer
    # and if the event is a busted event, associate the event with a buster.


    @transaction.commit_on_success
    def handle(self, *args, **options):
        pass
               
        print "Starting database population"
        
        userDataText = """\
           Tim, password123, Tim, Brooks, 123@123.com, profileImg/tolias.jpg, qrCodePath
           Hayden, password123, Hayden, Eskriett, 321@123.com, profileImg/IMG_4524.JPG, qrCodePath
           Jamie, password123, Jamie, Brooks, jb@123.com, profileImg/IMG_4199.JPG, qrCodePath
           Will, password123, Will, Buss, wb@123.com, profileImg/IMG_3793.JPG, qrCodePath
           Simon, password123, Simon, Bidwell, sb@123.com, profileImg/1B45B028-F8F9-4582-91AC-73EA5720B97E_1.jpg, qrCodePath
        """
        
        userData = []
        lines = iter( userDataText.split("\n") )
        for line in lines:
            try:
                info = line.split(',')
                info = [x.strip().rstrip() for x in info]
                if len(info) == 7:
                    userData.append(info)
            except StopIteration:
                pass
          
            
        for (username, password, firstName, surname, email, profileImage, qrCode) in userData:
            user = User(username=username, first_name=firstName, last_name=surname, email=email)
            user.set_password(password)
            user.save()
            userProfile = UserProfile(user=user, image=profileImage)
            userProfile.save()
            qr = QRCode(version=4, error_correction=ERROR_CORRECT_H)
            qr.add_data(ROOT_URL + "bust/" + username)
            qr.make() # Generate the QRCode itself
            im = qr.make_image()  #im contains a PIL.Image.Image object
            image_buffer = StringIO() #Save image to string buffer
            im.save(image_buffer)
            image_buffer.seek(0)   
            file_name = username+".png"
            file_object = File(image_buffer, file_name)
            content_file = ContentFile(file_object.read())
            userProfile.qrcode.save(file_name, content_file, save=True)
            userProfile.save()
            comp = Computer(name=username+"PC", url=username+"computer", owner=userProfile)
            comp.save()
              
        now = datetime.utcnow().replace(tzinfo=utc)
        
        for comp in Computer.objects.all():
            turnOnTime = random.randrange(-30, +30)
            turnOnDateTime = datetime(now.year, now.month, now.day, 9,0,0,1, pytz.UTC) + timedelta(minutes=turnOnTime)
            turnOnEvent = Event(eventType="CO", forComputer=comp, timestamp=turnOnDateTime)
            turnOnEvent.save()
            currentTimeStamp = turnOnDateTime + timedelta(seconds=2)
            previousEvent = ""
            while currentTimeStamp < turnOnDateTime + timedelta(hours=8):
                eventType = random.choice(['CA', 'CI'])
                timeInterval = random.randrange(0, 45)
                intervalStart = currentTimeStamp
                if eventType == 'CA':
                    event = Event(eventType=eventType, forComputer=comp, timestamp=currentTimeStamp)
                    event.save()
                    currentTimeStamp = currentTimeStamp +timedelta(seconds=2)
                    while currentTimeStamp < intervalStart + timedelta(minutes=timeInterval):
                        event = Event(eventType=eventType, forComputer=comp, timestamp=currentTimeStamp)
                        event.save()
                        currentTimeStamp = currentTimeStamp +timedelta(seconds=2)
                    previousEvent=eventType
                else:
                    if previousEvent == 'CI':
                        eventType = 'CA'
                        event = Event(eventType=eventType, forComputer=comp, timestamp=currentTimeStamp)
                        event.save()
                        currentTimeStamp = currentTimeStamp +timedelta(seconds=2)
                        while currentTimeStamp < intervalStart + timedelta(minutes=timeInterval):
                            event = Event(eventType=eventType, forComputer=comp, timestamp=currentTimeStamp)
                            event.save()
                            currentTimeStamp = currentTimeStamp +timedelta(seconds=2)
                    else:
                        event = Event(eventType=eventType, forComputer=comp, timestamp=currentTimeStamp)
                        event.save()
                        currentTimeStamp = currentTimeStamp +timedelta(seconds=2)
                        captured = False
                        capturer = None
                        while currentTimeStamp < intervalStart + timedelta(minutes=timeInterval):
                            if captured:
                                event = Event(eventType=eventType, forComputer=comp, timestamp=currentTimeStamp, bustedBy=capturer)
                                event.save()
                                currentTimeStamp = currentTimeStamp +timedelta(seconds=2)
                            else:
                                randomint = random.randrange(0, 1000)
                                if randomint == 0:   
                                    capturer = random.choice(UserProfile.objects.all().exclude(user=comp.owner.user))
                                    captured = True;
                                    event = Event(eventType=eventType, forComputer=comp, timestamp=currentTimeStamp, bustedBy=capturer)
                                    event.save()
                                    currentTimeStamp = currentTimeStamp +timedelta(seconds=2)
                                else:
                                    event = Event(eventType=eventType, forComputer=comp, timestamp=currentTimeStamp)
                                    event.save()
                                    currentTimeStamp = currentTimeStamp +timedelta(seconds=2)
                    previousEvent=eventType
        
        print "Populating finished."